import React from "react";
import PropTypes from "prop-types";
import {
  View,
  Dimensions,
  TouchableWithoutFeedback,
  Animated
} from "react-native";

import Card from "../Models/Card";
import Point from "../Models/Point";

export const CONST_CARD_SIZE_MODIFIER = 0.08;
export const CONST_DEFAULT_ANIMATION_TIMEOUT_MS = 250;
class CardView extends React.Component {
  constructor(props) {
    super(props);
    this.cardWidth = Dimensions.get("window").width * CONST_CARD_SIZE_MODIFIER;
    this.cardHeight = this.cardWidth * 3 / 2;
    const cardPosX = this.props.position
      ? this.props.position.x
      : Dimensions.get("window").width / 2 - this.cardWidth / 2;
    const cardPosY = this.props.position
      ? this.props.position.y
      : Dimensions.get("window").height / 2 - this.cardHeight / 2;
    this.state = {
      cardPosX,
      cardPosY
    };
    this.imageBack = this.props.cardImageLoader.cardBack;
    this.imageFront = this.props.cardImageLoader[
      `card${this.props.card.toString()}`
    ];
    this.flipAnimatedValue = new Animated.Value(
      this.props.isFacingUp ? 180 : 0
    );
    this.flipAnimatedInterpolate = this.flipAnimatedValue.interpolate({
      inputRange: [0, 90, 180],
      outputRange: ["0deg", "90deg", "0deg"]
    });
    this.cardPosXAnimatedValue = new Animated.Value(cardPosX);
    this.cardPosYAnimatedValue = new Animated.Value(cardPosY);
  }

  componentDidMount() {
    this.cardPosXAnimatedValue.addListener(({ value }) => {
      this.setState({ cardPosX: value });
    });
    this.cardPosYAnimatedValue.addListener(({ value }) => {
      this.setState({ cardPosY: value });
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isFacingUp !== this.props.isFacingUp) {
      this.animateFlipCard(nextProps.isFacingUp);
    }
    if (
      typeof nextProps.position !== typeof undefined &&
      (typeof this.props.position === typeof undefined ||
        nextProps.position.x !== this.props.position.x)
    ) {
      // console.log("[nextProps]: ", nextProps.position);
      this.animatePositionX(nextProps.position.x);
    }
    if (
      typeof nextProps.position !== typeof undefined &&
      (typeof this.props.position === typeof undefined ||
        nextProps.position.y !== this.props.position.y)
    ) {
      this.animatePositionY(nextProps.position.y);
    }
  }

  componentWillUnmount() {
    this.cardPosXAnimatedValue.stopAnimation();
    this.cardPosYAnimatedValue.stopAnimation();
    this.flipAnimatedValue.stopAnimation();
    this.cardPosXAnimatedValue.removeAllListeners();
    this.cardPosYAnimatedValue.removeAllListeners();
  }

  animateFlipCard = isFacingUp => {
    Animated.timing(this.flipAnimatedValue, {
      toValue: isFacingUp ? 180 : 0,
      duration: CONST_DEFAULT_ANIMATION_TIMEOUT_MS
    }).start();
  };

  animatePositionX = x => {
    Animated.timing(this.cardPosXAnimatedValue, {
      toValue: x,
      duration: CONST_DEFAULT_ANIMATION_TIMEOUT_MS
    }).start();
  };

  animatePositionY = y => {
    Animated.spring(this.cardPosYAnimatedValue, {
      toValue: y,
      duration: CONST_DEFAULT_ANIMATION_TIMEOUT_MS
    }).start();
  };

  render() {
    // console.log(`[render] ${this.props.card.hash}: `, this.state);
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          console.info("[TouchableWithoutFeedback] pressed");
          if (this.props.isUserInteractable && this.props.onCardTouchHandler) {
            this.props.onCardTouchHandler(!this.props.isOffsetted);
          }
        }}
      >
        <View
          style={{
            width: this.cardWidth,
            height: this.cardHeight,
            position: "absolute",
            top: this.props.isOffsetted
              ? this.state.cardPosY - this.cardHeight * 0.3
              : this.state.cardPosY,
            left: this.state.cardPosX,
            zIndex: this.props.zIndex
          }}
        >
          <Animated.Image
            source={this.props.isFacingUp ? this.imageFront : this.imageBack}
            style={{
              width: this.cardWidth,
              height: this.cardHeight,
              resizeMode: "contain",
              transform: [
                { rotateY: this.flipAnimatedInterpolate },
                { perspective: 1000 }
              ]
            }}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

CardView.propTypes = {
  cardImageLoader: PropTypes.object.isRequired,
  card: PropTypes.instanceOf(Card).isRequired,
  isOffsetted: PropTypes.bool,
  isFacingUp: PropTypes.bool,
  isUserInteractable: PropTypes.bool,
  zIndex: PropTypes.number.isRequired,
  onCardTouchHandler: PropTypes.func,
  position: PropTypes.instanceOf(Point)
};

export default CardView;
