export default function awaitableTimeout(ms, cancelPromise) {
  let timeoutHandle;
  return Promise.race([
    new Promise(resolve => setTimeout(resolve, ms)),
    (cancelPromise || new Promise(() => {})).then(() =>
      clearTimeout(timeoutHandle)
    )
  ]);
}
