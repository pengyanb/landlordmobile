const CardImageLoader = {
  card1Club: require("./Images/Cards/1Club.png"),
  card1Diamond: require("./Images/Cards/1Diamond.png"),
  card1Heart: require("./Images/Cards/1Heart.png"),
  card1Spade: require("./Images/Cards/1Spade.png"),

  card2Club: require("./Images/Cards/2Club.png"),
  card2Diamond: require("./Images/Cards/2Diamond.png"),
  card2Heart: require("./Images/Cards/2Heart.png"),
  card2Spade: require("./Images/Cards/2Spade.png"),

  card3Club: require("./Images/Cards/3Club.png"),
  card3Diamond: require("./Images/Cards/3Diamond.png"),
  card3Heart: require("./Images/Cards/3Heart.png"),
  card3Spade: require("./Images/Cards/3Spade.png"),

  card4Club: require("./Images/Cards/4Club.png"),
  card4Diamond: require("./Images/Cards/4Diamond.png"),
  card4Heart: require("./Images/Cards/4Heart.png"),
  card4Spade: require("./Images/Cards/4Spade.png"),

  card5Club: require("./Images/Cards/5Club.png"),
  card5Diamond: require("./Images/Cards/5Diamond.png"),
  card5Heart: require("./Images/Cards/5Heart.png"),
  card5Spade: require("./Images/Cards/5Spade.png"),

  card6Club: require("./Images/Cards/6Club.png"),
  card6Diamond: require("./Images/Cards/6Diamond.png"),
  card6Heart: require("./Images/Cards/6Heart.png"),
  card6Spade: require("./Images/Cards/6Spade.png"),

  card7Club: require("./Images/Cards/7Club.png"),
  card7Diamond: require("./Images/Cards/7Diamond.png"),
  card7Heart: require("./Images/Cards/7Heart.png"),
  card7Spade: require("./Images/Cards/7Spade.png"),

  card8Club: require("./Images/Cards/8Club.png"),
  card8Diamond: require("./Images/Cards/8Diamond.png"),
  card8Heart: require("./Images/Cards/8Heart.png"),
  card8Spade: require("./Images/Cards/8Spade.png"),

  card9Club: require("./Images/Cards/9Club.png"),
  card9Diamond: require("./Images/Cards/9Diamond.png"),
  card9Heart: require("./Images/Cards/9Heart.png"),
  card9Spade: require("./Images/Cards/9Spade.png"),

  card10Club: require("./Images/Cards/10Club.png"),
  card10Diamond: require("./Images/Cards/10Diamond.png"),
  card10Heart: require("./Images/Cards/10Heart.png"),
  card10Spade: require("./Images/Cards/10Spade.png"),

  card11Club: require("./Images/Cards/11Club.png"),
  card11Diamond: require("./Images/Cards/11Diamond.png"),
  card11Heart: require("./Images/Cards/11Heart.png"),
  card11Spade: require("./Images/Cards/11Spade.png"),

  card12Club: require("./Images/Cards/12Club.png"),
  card12Diamond: require("./Images/Cards/12Diamond.png"),
  card12Heart: require("./Images/Cards/12Heart.png"),
  card12Spade: require("./Images/Cards/12Spade.png"),

  card13Club: require("./Images/Cards/13Club.png"),
  card13Diamond: require("./Images/Cards/13Diamond.png"),
  card13Heart: require("./Images/Cards/13Heart.png"),
  card13Spade: require("./Images/Cards/13Spade.png"),

  cardjokerMono: require("./Images/Cards/jokerMono.png"),
  cardjokerColor: require("./Images/Cards/jokerColor.png"),
  cardBack: require("./Images/Cards/CardBack.png")
};

export default CardImageLoader;
