import {
  ACTION_SET_APP_LOADING_STATUS,
  ACTION_SET_APP_ERROR_INFO,
  ACTION_SET_BACKGROUND_MUSIC_FLAG,
  ACTION_SET_SOUND_EFFECT_FLAG
} from "../Constants/index";

export function actionSetAppLoadingStatus(status) {
  return {
    type: ACTION_SET_APP_LOADING_STATUS,
    status
  };
}

export function actionSetAppErrorInfo(error, message) {
  return {
    type: ACTION_SET_APP_ERROR_INFO,
    error,
    message
  };
}

export function actionSetBackgroundMusicFlag(flag) {
  return {
    type: ACTION_SET_BACKGROUND_MUSIC_FLAG,
    flag
  };
}

export function actionSetSoundEffectFlag(flag) {
  return {
    type: ACTION_SET_SOUND_EFFECT_FLAG,
    flag
  };
}
