// @ts-check
import {
  ACTION_SET_GAME_TYPE,
  ACTION_SET_GAME_PHASE
} from "../Constants/index";
/**
 *
 *
 * @export
 * @param {boolean} gameType
 * @returns
 */
export function actionSetGameType(gameType) {
  return {
    type: ACTION_SET_GAME_TYPE,
    gameType
  };
}
/**
 *
 *
 * @export
 * @param {string} gamePhase
 * @returns
 */
export function actionSetGamePhase(gamePhase) {
  return {
    type: ACTION_SET_GAME_PHASE,
    gamePhase
  };
}
