// // @ts-check
// import _ from "lodash";
// import { put, takeLatest, all, call } from "redux-saga/effects";
// import { ACTION_CHANGE_CARDS_POSITION } from "../Constants/index";
// import {
//   actionSetChangeCardsPositionResult,
//   actionSetChangeCardsPositionCompleteFlag
// } from "../Actions/Game.actions";

// import awaitableTimeout from "../../Utils/AwaitableTimeout";

// export function* changeCardsPositionRequest(action) {
//   const cardPositionDictionary = action.cardPositionDictionary;
//   const currentCardsPosition = _.cloneDeep(action.currentCardsPosition);
//   // const playerIndex = _.cloneDeep(action.playersIndex);
//   // const cardsIndexToChange = _.cloneDeep(action.cardsIndexToChange);

//   try {
//     yield put(actionSetChangeCardsPositionCompleteFlag(false));
//     const posPlayer3StackInfoArray =
//       cardPositionDictionary.posPlayer3StackInfo[
//         `${cardPositionDictionary.CONST_PLAYER_INITIAL_STACK_CARDS_COUNT}`
//       ];
//     if (
//       currentCardsPosition.playerStacks[0].length === 0 &&
//       currentCardsPosition.playerStacks[1].length === 0 &&
//       currentCardsPosition.playerStacks[2].length === 0
//     ) {
//       const cardsIndexArray = [];
//       for (
//         let i = 0;
//         i < cardPositionDictionary.CONST_PLAYER_INITIAL_STACK_CARDS_COUNT;
//         i++
//       ) {
//         cardsIndexArray.push(i);
//       }
//       yield all(
//         cardsIndexArray.map(function* moveCard(i) {
//           currentCardsPosition.playerStacks[0].push(
//             cardPositionDictionary.posPlayer1Stack
//           );
//           yield put(actionSetChangeCardsPositionResult(currentCardsPosition));
//           yield call(
//             awaitableTimeout,
//             cardPositionDictionary.CONST_DEFAULT_ANIMATION_TIMEOUT_MS
//           );
//           currentCardsPosition.playerStacks[1].push(
//             cardPositionDictionary.posPlayer2Stack
//           );
//           yield put(actionSetChangeCardsPositionResult(currentCardsPosition));
//           yield call(
//             awaitableTimeout,
//             cardPositionDictionary.CONST_DEFAULT_ANIMATION_TIMEOUT_MS
//           );
//           currentCardsPosition.playerStacks[2][i] = posPlayer3StackInfoArray[i];
//           console.log(`[posPlayer3_card${i}]: `, posPlayer3StackInfoArray[i]);
//           yield put(actionSetChangeCardsPositionResult(currentCardsPosition));
//           yield call(
//             awaitableTimeout,
//             cardPositionDictionary.CONST_DEFAULT_ANIMATION_TIMEOUT_MS
//           );
//         })
//       );
//     }
//     yield put(actionSetChangeCardsPositionCompleteFlag(true));
//   } catch (err) {
//     console.log("[changeCardsPositionRequest] err: ", err);
//   }
// }

// export function* changeCardsPositionWatcher() {
//   yield takeLatest(ACTION_CHANGE_CARDS_POSITION, changeCardsPositionRequest);
// }

// export default [changeCardsPositionWatcher];
