import { fromJS } from "immutable";
import {
  ACTION_SET_GAME_TYPE,
  ACTION_SET_GAME_PHASE
} from "../Constants/index";

const initialState = fromJS({
  gameType: "",
  gamePhase: "initial"
});

function reducerGame(state = initialState, action) {
  switch (action.type) {
    case ACTION_SET_GAME_TYPE:
      return state.set("gameType", action.gameType);
    case ACTION_SET_GAME_PHASE:
      return state.set("gamePhase", action.gamePhase);
    default:
      return state;
  }
}

export default reducerGame;
