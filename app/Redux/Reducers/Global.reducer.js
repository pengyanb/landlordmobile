import { fromJS } from "immutable";
import * as Constants from "../Constants/index";

const initialState = fromJS({
  appLoadingStatus: true,
  appErrorInfo: {
    error: false,
    message: ""
  },
  flagBackgroundMusic: true,
  flagEffectSound: true
});

function reducerGlobal(state = initialState, action) {
  switch (action.type) {
    case Constants.ACTION_SET_APP_LOADING_STATUS:
      return state.set("appLoadingStatus", action.status);
    case Constants.ACTION_SET_APP_ERROR_INFO:
      return state.set("appErrorInfo", {
        error: action.error,
        message: action.message
      });
    case Constants.ACTION_SET_BACKGROUND_MUSIC_FLAG:
      return state.set("flagBackgroundMusic", action.flag);
    case Constants.ACTION_SET_SOUND_EFFECT_FLAG:
      return state.set("flagEffectSound", action.flag);
    default:
      return state;
  }
}

export default reducerGlobal;
