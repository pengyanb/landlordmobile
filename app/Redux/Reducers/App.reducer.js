import { combineReducers } from "redux-immutable";

import reducerGlobal from "./Global.reducer";
import reducerGame from "./Game.reducer";

const AppReducer = combineReducers({
  Global: reducerGlobal,
  Game: reducerGame
});

export default AppReducer;
