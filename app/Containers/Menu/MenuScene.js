import React from "react";
import PropTypes from "prop-types";
import { Image, Dimensions, AsyncStorage } from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import {
  View,
  Container,
  Content,
  Card,
  CardItem,
  Body,
  Button,
  Text
} from "native-base";
import Orientation from "react-native-orientation";
import { actionSetGameType } from "../../Redux/Actions/Game.actions";
import CardImageLoader from "../../Assets/CardImageLoader";
import { SoundPickUp } from "../../Assets/Sounds/SoundDictionary";

const IMAGE_SPLASH_CARD = require("../../Assets/Images/splashCard.jpg");

const CONST_FLAG_EFFECT_SOUND_KEY = "FightLandlord:flagEffectSoundKey";
class MenuScene extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orientation: ""
    };
  }
  componentDidMount() {
    Orientation.getOrientation((err, orientation) => {
      this.setState({ orientation });
    });
    Orientation.addOrientationListener(this.handleOrientationChange);
  }
  componentWillUnmount() {
    Orientation.removeOrientationListener(this.handleOrientationChange);
  }

  handleOrientationChange = orientation => {
    this.setState({ orientation });
  };

  render() {
    return (
      <Container>
        <Content>
          <View
            style={{
              height: Dimensions.get("window").height,
              flexDirection:
                this.state.orientation === "LANDSCAPE" ? "row" : "column",
              justifyContent: "center",
              backgroundColor: "white"
            }}
          >
            <Card
              style={{
                width:
                  this.state.orientation === "LANDSCAPE"
                    ? Dimensions.get("window").width * 0.6
                    : Dimensions.get("window").width,
                flexDirection:
                  this.state.orientation === "LANDSCAPE" ? "row" : "column",
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <CardItem>
                <Body style={{ flexDirection: "column", alignItems: "center" }}>
                  <Image
                    source={IMAGE_SPLASH_CARD}
                    style={{
                      resizeMode: "contain",
                      width:
                        this.state.orientation === "LANDSCAPE"
                          ? Dimensions.get("window").width * 0.55
                          : Dimensions.get("window").width * 0.95
                    }}
                  />
                </Body>
              </CardItem>
              {this.state.orientation === "LANDSCAPE"
                ? null
                : [
                  <CardItem key="menuOfflineGameButton">
                    <Body>
                      <Button
                        bordered
                        block
                        onPress={async () => {
                            this.props.setGameType("OfflineGame");
                            const flagEffectSoundKey = await AsyncStorage.getItem(
                              CONST_FLAG_EFFECT_SOUND_KEY
                            );
                            if (JSON.parse(flagEffectSoundKey)) {
                              SoundPickUp.play();
                            }
                            Actions.gameScene({
                              cardImageLoader: CardImageLoader
                            });
                          }}
                      >
                        <Text>Play Game</Text>
                      </Button>
                    </Body>
                  </CardItem>,
                  <CardItem key="menuSettingsButton">
                    <Body>
                      <Button
                        bordered
                        block
                        onPress={() => {
                            Actions.settingScene();
                          }}
                      >
                        <Text>Settings</Text>
                      </Button>
                    </Body>
                  </CardItem>
                  ]}
            </Card>
            {this.state.orientation === "LANDSCAPE" ? (
              <Card
                style={{
                  width: Dimensions.get("window").width * 0.4,
                  height: Dimensions.get("window").height,
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <CardItem key="menuOfflineGameButton">
                  <Body>
                    <Button
                      bordered
                      block
                      onPress={async () => {
                        this.props.setGameType("OfflineGame");
                        const flagEffectSoundKey = await AsyncStorage.getItem(
                          CONST_FLAG_EFFECT_SOUND_KEY
                        );
                        if (JSON.parse(flagEffectSoundKey)) {
                          SoundPickUp.play();
                        }
                        Actions.gameScene({ cardImageLoader: CardImageLoader });
                      }}
                    >
                      <Text>Play Game</Text>
                    </Button>
                  </Body>
                </CardItem>
                <CardItem key="menuSettingsButton">
                  <Body>
                    <Button
                      bordered
                      block
                      onPress={() => {
                        Actions.settingScene();
                      }}
                    >
                      <Text>Settings</Text>
                    </Button>
                  </Body>
                </CardItem>
              </Card>
            ) : null}
          </View>
        </Content>
      </Container>
    );
  }
}

MenuScene.propTypes = {
  setGameType: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  setGameType: gameType => dispatch(actionSetGameType(gameType))
});

export default connect(null, mapDispatchToProps)(MenuScene);
