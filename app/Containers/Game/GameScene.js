import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { createSelector, createStructuredSelector } from "reselect";
import { View, Dimensions, Image, Platform, AsyncStorage } from "react-native";
import { Button, Text, Icon, ActionSheet } from "native-base";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { AdMobBanner, AdMobRewarded } from "react-native-admob";

import { CONST_GAME_TYPE, CONST_GAME_PHASE } from "../../Models/CONSTANS";
import {
  default as LandlordGameModel,
  EnumDealCardsType,
  EnumDealCardsResult
} from "../../Models/LandlordGame";
import { default as HardcodePlayerModel } from "../../Models/HardcodePlayer";
import Card from "../../Models/Card";
import Point from "../../Models/Point";

import CardView, {
  CONST_CARD_SIZE_MODIFIER,
  CONST_DEFAULT_ANIMATION_TIMEOUT_MS
} from "../../Components/CardView";

import {
  SoundCardPlace,
  SoundPickUp,
  SoundGameOver,
  SoundLevelClear
} from "../../Assets/Sounds/SoundDictionary";

import { actionSetGamePhase } from "../../Redux/Actions/Game.actions";

import awaitableTimeout from "../../Utils/AwaitableTimeout";

const IMG_LANDLORD_HAT = require("../../Assets/Images/landlordHat.png");

const CONST_PLAYER_MAX_STACK_CARDS_COUNT = 20;
const CONST_PLAYER_INITIAL_STACK_CARDS_COUNT = 17;
const CONST_CARD_OFFSET_MODIFIER = 0.3;

const CONST_DEFAULT_PLAYER_TIMEOUT = 15;
const CONST_HARDCODE_PLAYER_ACTION_TIME = 14;

const DEFAULT_GAME_INITAL_STATE = {
  debugGameCardsInfo: [
    {
      card: new Card(14, 0),
      isFacingUp: false,
      isUserInteractable: true
    }
  ],
  cardsPositionInfo: {
    playerStacks: [{}, {}, {}],
    baseStack: {},
    dealedCards: {}
  },
  gamePhase: CONST_GAME_PHASE.gamePhaseInitial,
  landlordGameModel: null,
  playerModels: [],
  player1UiInfo: {
    timer: 0,
    message: ""
  },
  player2UiInfo: {
    timer: 0,
    message: ""
  },
  player3UiInfo: {
    message: "",
    flagUiControlBidPhaseVisibility: false,
    flagUiControlPlayPhaseVisibility: false,
    flagUiControlPlayPhaseGoButtonEnabled: false
  }
};
const CONST_INITIAL_PLAYER_POINTS = 1000;
const CONST_BASE_POINTS_PER_ROUND = 100;
const CONST_PLAYER_POINTS_KEY = "FightLandlord:PlayerPoints";
const CONST_FLAG_EFFECT_SOUND_KEY = "FightLandlord:flagEffectSoundKey";
const admobBannerUnitID =
  Platform.OS === "ios"
    ? "ca-app-pub-3199275288482759/1251480809"
    : "ca-app-pub-3199275288482759/6951380109";
const admobRewardedUnitID =
  Platform.OS === "ios"
    ? "ca-app-pub-3199275288482759/1951169016"
    : "ca-app-pub-3199275288482759/6305702211";

class GameScene extends React.Component {
  constructor(props) {
    super(props);
    this.state = _.cloneDeep(DEFAULT_GAME_INITAL_STATE);
    this.player1Timer = null;
    this.player2Timer = null;
    this.player3Timer = null;
  }

  componentDidMount() {
    AdMobRewarded.setAdUnitID(admobRewardedUnitID);
    AdMobRewarded.addEventListener("rewarded", this.rewardedHanlder);
    this.cardPositionDictionary = {
      CONST_PLAYER_INITIAL_STACK_CARDS_COUNT,
      CONST_PLAYER_MAX_STACK_CARDS_COUNT,
      CONST_DEFAULT_ANIMATION_TIMEOUT_MS
    };
    const windowWidth = Dimensions.get("window").width;
    const windowHeight = Dimensions.get("window").height;
    const cardWidth = windowWidth * CONST_CARD_SIZE_MODIFIER;
    const cardHeight = cardWidth * 3 / 2;
    const cardOffset = cardWidth * CONST_CARD_OFFSET_MODIFIER;
    const windowCenterX = windowWidth * 0.5;
    const windowCenterY = windowHeight * 0.5;
    const windowPaddingOffset = windowWidth * 0.02;

    this.cardWidth = cardWidth;
    this.cardHeight = cardHeight;
    this.windowWidth = windowWidth;
    this.windowHeight = windowHeight;
    this.windowCenterX = windowCenterX;
    this.windowCenterY = windowCenterY;
    this.windowPaddingOffset = windowPaddingOffset;

    this.UiControlPosPairButtonLeft = new Point(
      windowCenterX - cardHeight * 1.5,
      windowCenterY + cardHeight * 0.2
    );

    this.UiControlPosPairButtonRight = new Point(
      windowCenterX + cardHeight * 0.5,
      windowCenterY + cardHeight * 0.2
    );

    this.cardPositionDictionary.posBaseStack = [
      new Point(windowPaddingOffset, windowPaddingOffset + cardHeight * 0.3),
      new Point(
        windowPaddingOffset + cardOffset,
        windowPaddingOffset + cardHeight * 0.3
      ),
      new Point(
        windowPaddingOffset + cardOffset * 2,
        windowPaddingOffset + cardHeight * 0.3
      )
    ];

    this.cardPositionDictionary.posCenterStack = new Point(
      windowCenterX - cardWidth * 0.5,
      windowCenterY - cardHeight * 0.5
    );
    this.cardPositionDictionary.posPlayer1Stack = new Point(
      windowPaddingOffset,
      windowCenterY - cardHeight * 0.5
    );
    this.cardPositionDictionary.posPlayer1CardCounter = new Point(
      windowPaddingOffset + cardWidth * 1.2,
      windowCenterY - cardHeight * 0.5
    );
    this.cardPositionDictionary.posPlayer2Stack = new Point(
      windowWidth - windowPaddingOffset - cardWidth,
      windowCenterY - cardHeight * 0.5
    );
    this.cardPositionDictionary.posPlayer2CardCounter = new Point(
      windowWidth - windowPaddingOffset - cardWidth * 1.5,
      windowCenterY - cardHeight * 0.5
    );

    this.cardPositionDictionary.posPlayer3StackInfo = {};
    for (let i = 0; i < CONST_PLAYER_MAX_STACK_CARDS_COUNT; i++) {
      const cardCount = i + 1;
      this.cardPositionDictionary.posPlayer3StackInfo[`${cardCount}`] = [];
      const isEvenCount = cardCount % 2 === 0;

      for (let j = 0; j < cardCount; j++) {
        const offsetIndex = isEvenCount
          ? j - cardCount / 2
          : j - (cardCount - 1) / 2;
        this.cardPositionDictionary.posPlayer3StackInfo[`${cardCount}`].push(
          new Point(
            windowCenterX + cardOffset * offsetIndex - cardWidth * 0.5,
            windowHeight - windowPaddingOffset - cardHeight
          )
        );
      }
    }

    // console.log("[this.cardPositionDictionary]: ", this.cardPositionDictionary);
    this.getPlayerPoints();
  }

  componentWillUnmount() {
    AdMobRewarded.removeEventListener("rewarded", this.rewardedHanlder);
  }

  setStateAsync = state =>
    new Promise(resolve => {
      this.setState(state, resolve);
    });

  getPlayerPoints = async () => {
    try {
      let playerPoints = await AsyncStorage.getItem(CONST_PLAYER_POINTS_KEY);

      if (playerPoints === null) {
        playerPoints = CONST_INITIAL_PLAYER_POINTS;
        await AsyncStorage.setItem(
          CONST_PLAYER_POINTS_KEY,
          `${CONST_INITIAL_PLAYER_POINTS}`
        );
        this.setState({
          playerPoints: `${CONST_INITIAL_PLAYER_POINTS}`
        });
      } else {
        this.setState({ playerPoints });
        playerPoints = parseInt(playerPoints, 10);
      }

      const flagEffectSound = await AsyncStorage.getItem(
        CONST_FLAG_EFFECT_SOUND_KEY
      );
      if (flagEffectSound !== null) {
        this.setState({ flagEffectSound: JSON.parse(flagEffectSound) });
      }
      // console.log("[playerPoints]: ", playerPoints);
      if (playerPoints > 0) {
        if (
          this.props.GameState.gameType === CONST_GAME_TYPE.gameTypeOffline &&
          this.state.gamePhase === CONST_GAME_PHASE.gamePhaseInitial
        ) {
          this.offlineGameLoop();
        }
      }
    } catch (err) {
      console.log("[getPlayerPoints]: ", err);
    }
  };

  setPlayerPoints = async newPoints => {
    try {
      await AsyncStorage.setItem(CONST_PLAYER_POINTS_KEY, `${newPoints}`);
      this.setState({ playerPoints: `${newPoints}` });
    } catch (err) {
      console.log("[setPlayerPoints]: ", err);
    }
  };

  rewardedHanlder = async () => {
    let playerPoints = await AsyncStorage.getItem(CONST_PLAYER_POINTS_KEY);
    playerPoints = parseInt(playerPoints, 10);
    // console.log("[rewardedHandler]: ", playerPoints);
    this.setPlayerPoints(playerPoints + 500);
  };

  initOfflineGame = () => {
    const landlordGameModel = new LandlordGameModel();
    landlordGameModel.modelStacksChangeEventHandler = this.modelStacksChangeEventHandler;
    this.setState(
      Object.assign({}, _.cloneDeep(DEFAULT_GAME_INITAL_STATE), {
        landlordGameModel,
        playerModels: [
          new HardcodePlayerModel(),
          new HardcodePlayerModel(),
          null
        ],
        gamePhase: CONST_GAME_PHASE.gamePhaseDistributingCard
      }),
      () => {
        this.initCardsPosition();
      }
    );
  };

  initCardsPosition = () => {
    const landlordGameModel = this.state.landlordGameModel;

    const cardPositionDictionary = this.cardPositionDictionary;
    const currentCardsPosition = {
      playerStacks: [{}, {}, {}],
      baseStack: {},
      dealedCards: {}
    };
    const posPlayer3StackInfoArray =
      cardPositionDictionary.posPlayer3StackInfo[
        `${cardPositionDictionary.CONST_PLAYER_INITIAL_STACK_CARDS_COUNT}`
      ];
    landlordGameModel.playerStacks[0]
      .slice(0) // map a shallow copy then reverse
      .reverse()
      .map(card => {
        currentCardsPosition.playerStacks[0][card.hash] = {
          position: cardPositionDictionary.posPlayer1Stack,
          offset: false,
          facingUp: false
        };
      });
    landlordGameModel.playerStacks[1]
      .slice(0) // map a shallow copy then reverse
      .reverse()
      .map(card => {
        currentCardsPosition.playerStacks[1][card.hash] = {
          position: cardPositionDictionary.posPlayer2Stack,
          offset: false,
          facingUp: false
        };
      });
    landlordGameModel.playerStacks[2]
      .slice(0) // map a shallow copy then reverse
      .reverse()
      .map((card, index) => {
        currentCardsPosition.playerStacks[2][card.hash] = {
          position: posPlayer3StackInfoArray[index],
          offset: false,
          facingUp: true
        };
      });
    landlordGameModel.baseStack
      .slice(0) // map a shallow copy then reverse
      .reverse()
      .map(card => {
        currentCardsPosition.baseStack[card.hash] = {
          position: cardPositionDictionary.posCenterStack,
          facingUp: false
        };
      });

    this.setState({ cardsPositionInfo: currentCardsPosition }, () => {
      this.setState({ gamePhase: CONST_GAME_PHASE.gamePhaseBid }, () => {
        this.offlineGameLoop();
      });
    });
  };

  modelStacksChangeEventHandler = async (stackIndex, dealedCards) => {
    // console.log("[modelStacksChangeEventHandler]", stackIndex);
    if (typeof stackIndex !== typeof undefined) {
      const landlordGameModel = this.state.landlordGameModel;
      const cardsPositionInfo = this.state.cardsPositionInfo;
      if (typeof dealedCards === typeof undefined) {
        // finish bid landlord, add baseStack to playerStack
        const playerStack = landlordGameModel.playerStacks[stackIndex];
        // console.log(`[modelStacksChangeEventHandler] player${stackIndex}Stack.length`, playerStack.length);
        const posPlayer3StackInfoArray = this.cardPositionDictionary
          .posPlayer3StackInfo[`${playerStack.length}`];
        playerStack
          .slice(0) // map a shallow copy then reverse
          .reverse()
          .map((card, index) => {
            if (stackIndex === 0) {
              cardsPositionInfo.playerStacks[0][card.hash] = {
                position: this.cardPositionDictionary.posPlayer1Stack,
                offset: false,
                facingUp: false
              };
            } else if (stackIndex === 1) {
              cardsPositionInfo.playerStacks[1][card.hash] = {
                position: this.cardPositionDictionary.posPlayer2Stack,
                offset: false,
                facingUp: false
              };
            } else if (stackIndex === 2) {
              cardsPositionInfo.playerStacks[2][card.hash] = {
                position: posPlayer3StackInfoArray[index],
                offset: false,
                facingUp: true
              };
            }
          });
        // console.log("[modelStacksChangeEventHandler]", playerStack);
        this.setState({ cardsPositionInfo, landlordGameModel });
        await awaitableTimeout(CONST_DEFAULT_ANIMATION_TIMEOUT_MS);
        if (this.state.flagEffectSound) {
          SoundCardPlace.play();
        }
      } else {
        // players deal cards
        if (!Array.isArray(dealedCards)) {
          // dealed a single card
          dealedCards = [dealedCards];
        }
        const posPlayer3StackInfoArray = this.cardPositionDictionary
          .posPlayer3StackInfo[`${dealedCards.length}`];
        const destinationCardsPos = {};
        const yPos = this.windowCenterY - this.cardHeight;
        cardsPositionInfo.dealedCards = {};
        dealedCards
          .slice(0)
          .reverse()
          .map((card, index) => {
            const pos = posPlayer3StackInfoArray[index];
            destinationCardsPos[card.hash] = new Point(pos.x, yPos);
            cardsPositionInfo.dealedCards[card.hash] = {
              position: new Point(pos.x, yPos),
              facingUp: true
            };
          });

        landlordGameModel.playerStacks[stackIndex]
          .slice(0)
          .reverse()
          .map(card => {
            if (typeof destinationCardsPos[card.hash] !== typeof undefined) {
              cardsPositionInfo.playerStacks[stackIndex][card.hash].position =
                destinationCardsPos[card.hash];
              cardsPositionInfo.playerStacks[stackIndex][
                card.hash
              ].facingUp = true;
            }
          });
        await this.setStateAsync({ cardsPositionInfo });
        await awaitableTimeout(CONST_DEFAULT_ANIMATION_TIMEOUT_MS);
        if (this.state.flagEffectSound) {
          SoundCardPlace.play();
        }
      }
    }
  };

  handlePlayerBidTimeout = () => {
    const bidLandlordTurn = this.state.landlordGameModel.bidLandlordTurn;
    // console.log("[handlePlayerBidTimeout]: ", bidLandlordTurn + 1);
    const done = () => {
      if (this.player1Timer) {
        clearTimeout(this.player1Timer);
        this.player1Timer = null;
      }
      if (this.player2Timer) {
        clearTimeout(this.player2Timer);
        this.player2Timer = null;
      }
      if (
        this.state.playerModels[this.state.landlordGameModel.bidLandlordTurn]
      ) {
        if (
          this.state.playerModels[
            this.state.landlordGameModel.bidLandlordTurn
          ].bid(
            this.state.landlordGameModel.playerStacks[
              this.state.landlordGameModel.bidLandlordTurn
            ]
          )
        ) {
          if (this.state.landlordGameModel.bidLandlordTurn === 0) {
            const player1UiInfo = this.state.player1UiInfo;
            player1UiInfo.message = "bid";
            const player2UiInfo = this.state.player2UiInfo;
            player2UiInfo.message = "";
            this.setState({ player1UiInfo });
            setTimeout(() => {
              this.setState({ player2UiInfo });
            }, 1000);
          } else if (this.state.landlordGameModel.bidLandlordTurn === 1) {
            const player2UiInfo = this.state.player2UiInfo;
            player2UiInfo.message = "bid";
            const player1UiInfo = this.state.player1UiInfo;
            player1UiInfo.message = "";
            this.setState({ player2UiInfo });
            setTimeout(() => {
              this.setState({ player1UiInfo });
            }, 1000);
          }
          this.gamePhaseHandlerBid("bid");
        } else {
          if (this.state.landlordGameModel.bidLandlordTurn === 0) {
            const player1UiInfo = this.state.player1UiInfo;
            player1UiInfo.message = "pass";
            const player2UiInfo = this.state.player2UiInfo;
            player2UiInfo.message = "";
            this.setState({ player1UiInfo });
            setTimeout(() => {
              this.setState({ player2UiInfo });
            }, 1000);
          } else if (this.state.landlordGameModel.bidLandlordTurn === 1) {
            const player2UiInfo = this.state.player2UiInfo;
            player2UiInfo.message = "pass";
            const player1UiInfo = this.state.player1UiInfo;
            player1UiInfo.message = "";
            this.setState({ player2UiInfo });
            setTimeout(() => {
              this.setState({ player1UiInfo });
            }, 1000);
          }
          this.gamePhaseHandlerBid("pass");
        }
      } else {
        this.gamePhaseHandlerBid();
      }
    };
    if (bidLandlordTurn === 0) {
      const player1UiInfo = this.state.player1UiInfo;
      if (player1UiInfo.timer > 0) {
        if (player1UiInfo.timer <= CONST_HARDCODE_PLAYER_ACTION_TIME) {
          player1UiInfo.timer = 0;
          this.player1Timer = null;
          this.setState({ player1UiInfo }, done);
        } else {
          this.player1Timer = setTimeout(() => {
            this.player1Timer = null;
            player1UiInfo.timer -= 1;
            this.setState({ player1UiInfo }, () => {
              this.handlePlayerBidTimeout();
            });
          }, 1000);
        }
      } else {
        player1UiInfo.timer = 0;
        this.setState({ player1UiInfo });
        done();
      }
    } else if (bidLandlordTurn === 1) {
      const player2UiInfo = this.state.player2UiInfo;
      if (player2UiInfo.timer > 0) {
        if (player2UiInfo.timer <= CONST_HARDCODE_PLAYER_ACTION_TIME) {
          player2UiInfo.timer = 0;
          this.player2Timer = null;
          this.setState({ player2UiInfo }, done);
        } else {
          this.player2Timer = setTimeout(() => {
            this.player2Timer = null;
            player2UiInfo.timer -= 1;
            this.setState({ player2UiInfo }, () => {
              this.handlePlayerBidTimeout();
            });
          }, 1000);
        }
      } else {
        player2UiInfo.timer = 0;
        this.setState({ player2UiInfo });
        done();
      }
    } else {
      done();
    }
  };

  handlePlayerPlayTimeout = () => {
    const landlordGameModel = this.state.landlordGameModel;
    const playerModels = this.state.playerModels;
    let previousPlayerIndex;
    let nextPlayerIndex;
    const playLandlordTurn = landlordGameModel.playLandlordTurn;
    // console.log("[handlePlayerPlayTimout]: ", playLandlordTurn + 1);
    const done = () => {
      // console.log("[handlePlayerPlayTimeout] done");
      if (this.player1Timer) {
        clearTimeout(this.player1Timer);
        this.player1Timer = null;
      }
      if (this.player2Timer) {
        clearTimeout(this.player2Timer);
        this.player2Timer = null;
      }
      if (playerModels[landlordGameModel.playLandlordTurn]) {
        previousPlayerIndex = landlordGameModel.playLandlordTurn - 1;
        nextPlayerIndex = landlordGameModel.playLandlordTurn + 1;
        if (previousPlayerIndex < 0) {
          previousPlayerIndex = 2;
        }
        if (nextPlayerIndex > 2) {
          nextPlayerIndex = 0;
        }
        const dealedCardInfo = playerModels[
          landlordGameModel.playLandlordTurn
        ].play(
          landlordGameModel.playLandlordTurn,
          landlordGameModel.landlord,
          landlordGameModel.previousDealedCardsInfo === null
            ? null
            : landlordGameModel.previousDealedCardsInfo.player ===
              landlordGameModel.playLandlordTurn
              ? null
              : landlordGameModel.previousDealedCardsInfo.cards,
          landlordGameModel.playerStacks[landlordGameModel.playLandlordTurn],
          landlordGameModel.playerStacks[previousPlayerIndex].length,
          landlordGameModel.playerStacks[nextPlayerIndex].length
        );
        if (Array.isArray(dealedCardInfo)) {
          if (landlordGameModel.playLandlordTurn === 0) {
            const player1UiInfo = this.state.player1UiInfo;
            player1UiInfo.message = "pass";
            const player2UiInfo = this.state.player2UiInfo;
            player2UiInfo.message = "";
            this.setState({ player1UiInfo });
            setTimeout(() => {
              this.setState({ player2UiInfo });
            }, 1000);
          } else if (landlordGameModel.playLandlordTurn === 1) {
            const player2UiInfo = this.state.player2UiInfo;
            player2UiInfo.message = "pass";
            const player1UiInfo = this.state.player1UiInfo;
            player1UiInfo.message = "";
            this.setState({ player2UiInfo });
            setTimeout(() => {
              this.setState({ player1UiInfo });
            }, 1000);
          }
          this.gamePhaseHandlerPlay(dealedCardInfo.map(cInfo => cInfo.hash));
        } else if (dealedCardInfo instanceof Card) {
          this.gamePhaseHandlerPlay([dealedCardInfo.hash]);
        } else {
          throw Error("Invalid dealedCardInfo");
        }
      } else {
        this.gamePhaseHandlerPlay();
      }
    };
    if (playLandlordTurn === 0) {
      const player1UiInfo = this.state.player1UiInfo;
      if (player1UiInfo.timer > 0) {
        if (player1UiInfo.timer <= CONST_HARDCODE_PLAYER_ACTION_TIME) {
          player1UiInfo.timer = 0;
          this.player1Timer = null;
          this.setState({ player1UiInfo }, done);
        } else {
          this.player1Timer = setTimeout(() => {
            this.player1Timer = null;
            player1UiInfo.timer -= 1;
            this.setState({ player1UiInfo }, () => {
              this.handlePlayerPlayTimeout();
            });
          }, 1000);
        }
      } else {
        player1UiInfo.timer = 0;
        this.setState({ player1UiInfo });
        done();
      }
    } else if (playLandlordTurn === 1) {
      const player2UiInfo = this.state.player2UiInfo;
      if (player2UiInfo.timer > 0) {
        if (player2UiInfo.timer <= CONST_HARDCODE_PLAYER_ACTION_TIME) {
          player2UiInfo.timer = 0;
          this.player2Timer = null;
          this.setState({ player2UiInfo }, done);
        } else {
          this.player2Timer = setTimeout(() => {
            this.player2Timer = null;
            player2UiInfo.timer -= 1;
            this.setState({ player2UiInfo }, () => {
              this.handlePlayerPlayTimeout();
            });
          }, 1000);
        }
      } else {
        player2UiInfo.timer = 0;
        player2UiInfo.flagUiControlPlayPhaseVisibility = false;
        player2UiInfo.flagUiControlPlayPhaseGoButtonEnabled = false;
        this.setState({ player2UiInfo });
        done();
      }
    } else {
      done();
    }
  };

  gamePhaseHandlerBid = async command => {
    const landlordGameModel = this.state.landlordGameModel;
    // console.log(
    //   `[gamePhaseHandlerBid] player ${landlordGameModel.bidLandlordTurn +
    //     1} command: `,
    //   command
    // );
    if (typeof command === typeof undefined) {
      const player3UiInfo = this.state.player3UiInfo;
      player3UiInfo.flagUiControlBidPhaseVisibility = true;
      this.setState({ player3UiInfo });
    } else {
      let bidResult;
      switch (command) {
        case "bid":
          // console.log(`[Player ${landlordGameModel.bidLandlordTurn + 1}] bid`);
          bidResult = await landlordGameModel.bidLandlord(true);
          if (bidResult) {
            this.offlineGameLoop();
          } else if (landlordGameModel.landlord !== null) {
            // console.log(`[Landlord]: player${landlordGameModel.landlord + 1}`);
            const gamePhase = CONST_GAME_PHASE.gamePhasePlay;
            landlordGameModel.playLandlordTurn = landlordGameModel.landlord;
            const cardsPositionInfo = this.state.cardsPositionInfo;
            landlordGameModel.baseStack
              .slice(0)
              .reverse()
              .map((card, index) => {
                cardsPositionInfo.baseStack[card.hash] = {
                  position: this.cardPositionDictionary.posBaseStack[index],
                  facingUp: true
                };
              });
            const player1UiInfo = this.state.player1UiInfo;
            const player2UiInfo = this.state.player2UiInfo;
            player1UiInfo.message = "";
            player2UiInfo.message = "";

            this.setState(
              {
                landlordGameModel,
                cardsPositionInfo,
                gamePhase,
                player1UiInfo,
                player2UiInfo
              },
              this.offlineGameLoop
            );
          } else {
            // console.log(
            //   `No one bid, start a new game\n================================\n`
            // );
            this.initOfflineGame();
          }
          break;
        case "pass":
          // console.log(`[Player ${landlordGameModel.bidLandlordTurn + 1}] pass`);
          bidResult = await landlordGameModel.bidLandlord(false);
          if (bidResult) {
            this.offlineGameLoop();
          } else {
            if (landlordGameModel.landlord !== null) {
              // console.log(
              //   `[landlord]: player${landlordGameModel.landlord + 1}`
              // );
              const gamePhase = CONST_GAME_PHASE.gamePhasePlay;
              landlordGameModel.playLandlordTurn = landlordGameModel.landlord;
              const cardsPositionInfo = this.state.cardsPositionInfo;
              landlordGameModel.baseStack
                .slice(0)
                .reverse()
                .map((card, index) => {
                  cardsPositionInfo.baseStack[card.hash] = {
                    position: this.cardPositionDictionary.posBaseStack[index],
                    facingUp: true
                  };
                });
              this.setState(
                { landlordGameModel, cardsPositionInfo, gamePhase },
                this.offlineGameLoop
              );
            } else {
              // console.log("No one bid, start a new game\n\n");
              this.initOfflineGame();
            }
          }
          break;
        default:
          this.initOfflineGame();
          break;
      }
    }
  };

  gamePhaseHandlerPlay = async dealedCardHashs => {
    const landlordGameModel = this.state.landlordGameModel;
    const currentPlayerIndex = landlordGameModel.playLandlordTurn;
    // console.log(
    //   "[gamePhasHandlerPlayer] currentPlayerIndex: ",
    //   currentPlayerIndex
    // );
    if (typeof dealedCardHashs === typeof undefined) {
      const player3UiInfo = this.state.player3UiInfo;
      player3UiInfo.flagUiControlPlayPhaseVisibility = true;
      player3UiInfo.flagUiControlPlayPhaseGoButtonEnabled = false;
      this.setState({ player3UiInfo });
    } else {
      if (landlordGameModel.playLandlordTurn === null) {
        landlordGameModel.playLandlordTurn = landlordGameModel.landlord;
      }

      let previousPlayerIndex = currentPlayerIndex - 1;
      let nextPlayerIndex = currentPlayerIndex + 1;
      if (previousPlayerIndex < 0) {
        previousPlayerIndex = 2;
      }
      if (nextPlayerIndex > 2) {
        nextPlayerIndex = 0;
      }
      // console.log("[dealedCardHashs]: ", dealedCardHashs);
      const dealedCardsResult = await landlordGameModel.dealCards(
        dealedCardHashs
      );
      if (dealedCardsResult !== EnumDealCardsResult.GAME_FINISHED) {
        // console.log("[dealedCardsResult]: ", dealedCardsResult);
        await this.setStateAsync({ landlordGameModel });
        await this.modelStacksChangeEventHandler(currentPlayerIndex);
        this.offlineGameLoop();
      } else {
        const gamePhase = CONST_GAME_PHASE.gamePhaseEnd;
        await this.setStateAsync({ landlordGameModel, gamePhase });
        this.offlineGameLoop();
      }
    }
  };

  offlineGameLoop = () => {
    // console.log("[offlineGameLoop]: ", this.state.gamePhase);
    let playerWin = false;
    let playerPoints;
    let scoreVariation;
    switch (this.state.gamePhase) {
      case CONST_GAME_PHASE.gamePhaseInitial:
        this.initOfflineGame();
        break;
      case CONST_GAME_PHASE.gamePhaseBid:
        if (this.state.landlordGameModel.bidLandlordTurn === 0) {
          const player1UiInfo = this.state.player1UiInfo;
          player1UiInfo.timer = CONST_DEFAULT_PLAYER_TIMEOUT;
          const player2UiInfo = this.state.player2UiInfo;
          player2UiInfo.message = "";
          this.setState({ player1UiInfo }, this.handlePlayerBidTimeout);
          setTimeout(() => {
            this.setState({ player2UiInfo });
          }, 1000);
        } else if (this.state.landlordGameModel.bidLandlordTurn === 1) {
          const player2UiInfo = this.state.player2UiInfo;
          player2UiInfo.timer = CONST_DEFAULT_PLAYER_TIMEOUT;
          const player1UiInfo = this.state.player1UiInfo;
          player1UiInfo.message = "";
          this.setState({ player2UiInfo }, this.handlePlayerBidTimeout);
          setTimeout(() => {
            this.setState({ player1UiInfo });
          }, 1000);
        } else {
          this.handlePlayerBidTimeout();
        }
        break;
      case CONST_GAME_PHASE.gamePhasePlay:
        if (this.state.landlordGameModel.playLandlordTurn === 0) {
          const player1UiInfo = this.state.player1UiInfo;
          player1UiInfo.timer = CONST_DEFAULT_PLAYER_TIMEOUT;
          const player2UiInfo = this.state.player2UiInfo;
          player2UiInfo.message = "";
          this.setState({ player1UiInfo }, this.handlePlayerPlayTimeout);
          setTimeout(() => {
            this.setState({ player2UiInfo });
          }, 1000);
        } else if (this.state.landlordGameModel.playLandlordTurn === 1) {
          const player2UiInfo = this.state.player2UiInfo;
          player2UiInfo.timer = CONST_DEFAULT_PLAYER_TIMEOUT;
          const player1UiInfo = this.state.player1UiInfo;
          player1UiInfo.message = "";
          this.setState({ player2UiInfo }, this.handlePlayerPlayTimeout);
          setTimeout(() => {
            this.setState({ player1UiInfo });
          }, 1000);
        } else {
          this.handlePlayerPlayTimeout();
          this.playerPhaseGoButtonEnableHandler();
        }
        break;
      case CONST_GAME_PHASE.gamePhaseEnd:
        if (this.state.landlordGameModel.winner === 2) {
          playerWin = true;
        } else {
          if (this.state.landlordGameModel.landlord === 2) {
            playerWin = false;
          } else {
            if (
              this.state.landlordGameModel.landlord ===
              this.state.landlordGameModel.winner
            ) {
              playerWin = false;
            } else {
              playerWin = true;
            }
          }
        }
        playerPoints = parseInt(this.state.playerPoints, 10);
        scoreVariation =
          this.state.landlordGameModel.scoreMultiplier *
          CONST_BASE_POINTS_PER_ROUND;
        if (playerWin) {
          if (this.state.flagEffectSound) {
            SoundLevelClear.play();
          }
          playerPoints += scoreVariation;
        } else {
          if (this.state.flagEffectSound) {
            SoundGameOver.play();
          }
          playerPoints -= scoreVariation;
        }
        this.setPlayerPoints(playerPoints);
        ActionSheet.show(
          {
            options:
              playerPoints > 0
                ? ["New Game", "Get more points", "Quit"]
                : ["Get more points", "Quit"],
            title: `You ${
              playerWin ? `win +${scoreVariation}` : `lose -${scoreVariation}`
            }`
          },
          async buttonIndex => {
            if (playerPoints > 0) {
              if (buttonIndex === 0) {
                this.initOfflineGame();
              } else if (buttonIndex === 1) {
                AdMobRewarded.requestAd().then(() => {
                  AdMobRewarded.showAd();
                });
              } else {
                if (
                  this.state.gamePhase === CONST_GAME_PHASE.gamePhaseBid ||
                  this.state.gamePhase === CONST_GAME_PHASE.gamePhasePlay
                ) {
                  await AsyncStorage.setItem(
                    CONST_PLAYER_POINTS_KEY,
                    JSON.stringify(playerPoints - 300)
                  );
                }
                Actions.menuScene();
              }
            } else {
              if (buttonIndex === 0) {
                AdMobRewarded.requestAd().then(() => {
                  AdMobRewarded.showAd();
                });
              } else {
                if (
                  this.state.gamePhase === CONST_GAME_PHASE.gamePhaseBid ||
                  this.state.gamePhase === CONST_GAME_PHASE.gamePhasePlay
                ) {
                  await AsyncStorage.setItem(
                    CONST_PLAYER_POINTS_KEY,
                    JSON.stringify(playerPoints - 300)
                  );
                }
                Actions.menuScene();
              }
            }
          }
        );
        break;
      default:
        break;
    }
  };

  playerPhaseGoButtonEnableHandler = () => {
    const cardsToDeal = [];
    const landlordGameModel = this.state.landlordGameModel;
    const player3UiInfo = this.state.player3UiInfo;
    landlordGameModel.playerStacks[2].map(checkCard => {
      if (checkCard.userSelected) {
        cardsToDeal.push(checkCard);
      }
    });
    // console.log("[playerPhaseGoButtonEnableHandler]: ", cardsToDeal);
    if (cardsToDeal.length > 0) {
      const cardsToDealType = LandlordGameModel.getDealedCardsType(cardsToDeal);
      if (landlordGameModel.previousDealedCardsInfo) {
        if (cardsToDealType !== EnumDealCardsType.INVALID) {
          if (
            LandlordGameModel.compareCardStacks(
              landlordGameModel.previousDealedCardsInfo,
              {
                cardsType: cardsToDealType,
                cards: cardsToDeal
              }
            )
          ) {
            player3UiInfo.flagUiControlPlayPhaseGoButtonEnabled = true;
          } else {
            player3UiInfo.flagUiControlPlayPhaseGoButtonEnabled = false;
          }
        } else {
          player3UiInfo.flagUiControlPlayPhaseGoButtonEnabled = false;
        }
      } else {
        if (cardsToDealType !== EnumDealCardsType.INVALID) {
          player3UiInfo.flagUiControlPlayPhaseGoButtonEnabled = true;
        } else {
          player3UiInfo.flagUiControlPlayPhaseGoButtonEnabled = false;
        }
      }
    } else {
      player3UiInfo.flagUiControlPlayPhaseGoButtonEnabled = false;
    }
    this.setState({ player3UiInfo });
  };

  renderOfflineGame = () => {
    // console.log("[OfflineGame]: ", this.props.GameState);
    const landlordGameModel = this.state.landlordGameModel;
    // console.log(
    //   "[renderOfflineGame] baseStack: ",
    //   landlordGameModel
    //     ? `${landlordGameModel.baseStack.length}, ${
    //         landlordGameModel.playerStacks[0].length
    //       }, ${landlordGameModel.playerStacks[1].length}, ${
    //         landlordGameModel.playerStacks[2].length
    //       }`
    //     : "null"
    // );
    // console.log(
    //   "[renderOfflineGame] pos: ",
    //   this.state.cardsPositionInfo.playerStacks[2]
    // );
    const cardsPositionInfo = this.state.cardsPositionInfo;
    if (
      Object.keys(cardsPositionInfo.playerStacks[0]).length > 0 &&
      Object.keys(cardsPositionInfo.playerStacks[1]).length > 0 &&
      Object.keys(cardsPositionInfo.playerStacks[2]).length > 0
    ) {
      let dealedCards = [];
      if (
        landlordGameModel &&
        landlordGameModel.previousDealedCardsInfo &&
        landlordGameModel.previousDealedCardsInfo.cards
      ) {
        dealedCards = landlordGameModel.previousDealedCardsInfo.cards;
        if (!Array.isArray(dealedCards)) {
          dealedCards = [dealedCards];
        }
      }
      // console.log("[!!!dealedCards!!!]: ", dealedCards);
      // console.log(
      //   "[cardsPostionInfo.dealedCards]: ",
      //   cardsPositionInfo.dealedCards
      // );
      return (
        <View
          style={{
            position: "relative",
            width: Dimensions.get("window").width,
            height: Dimensions.get("window").height,
            backgroundColor: "#009688"
          }}
        >
          {landlordGameModel
            ? [
                ...landlordGameModel.baseStack.map((card, index) => (
                  <CardView
                    key={`baseCard${card.hash}`}
                    card={card}
                    cardImageLoader={this.props.cardImageLoader}
                    isFacingUp={cardsPositionInfo.baseStack[card.hash].facingUp}
                    zIndex={landlordGameModel.baseStack.length - index}
                    position={cardsPositionInfo.baseStack[card.hash].position}
                  />
                )),
                ...landlordGameModel.playerStacks[0].map((card, index) => (
                  <CardView
                    key={`player1Card${card.hash}`}
                    card={card}
                    cardImageLoader={this.props.cardImageLoader}
                    isFacingUp={
                      cardsPositionInfo.playerStacks[0][card.hash].facingUp
                    }
                    zIndex={landlordGameModel.playerStacks[0].length - index}
                    position={
                      cardsPositionInfo.playerStacks[0][card.hash].position
                    }
                  />
                )),
                ...landlordGameModel.playerStacks[1].map((card, index) => (
                  <CardView
                    key={`player2Card${card.hash}`}
                    card={card}
                    cardImageLoader={this.props.cardImageLoader}
                    isFacingUp={
                      cardsPositionInfo.playerStacks[1][card.hash].facingUp
                    }
                    zIndex={landlordGameModel.playerStacks[1].length - index}
                    position={
                      cardsPositionInfo.playerStacks[1][card.hash].position
                    }
                  />
                )),
                ...landlordGameModel.playerStacks[2].map((card, index) => (
                  <CardView
                    key={`player3Card${card.hash}`}
                    card={card}
                    cardImageLoader={this.props.cardImageLoader}
                    isFacingUp
                    isUserInteractable
                    isOffsetted={card.userSelected}
                    zIndex={landlordGameModel.playerStacks[2].length - index}
                    onCardTouchHandler={async isOffsetted => {
                      if (this.state.flagEffectSound) {
                        SoundCardPlace.play();
                      }
                      landlordGameModel.playerStacks[2][
                        index
                      ].userSelected = isOffsetted;
                      await this.setStateAsync({ landlordGameModel });
                      this.playerPhaseGoButtonEnableHandler();
                    }}
                    position={
                      cardsPositionInfo.playerStacks[2][card.hash].position
                    }
                  />
                )),
                ...dealedCards.map((card, index) => {
                  // console.log("[dealedCard]: ", card);
                  // console.log(
                  //   `[Pos ${card.hash}]: `,
                  //   cardsPositionInfo.dealedCards[card.hash]
                  // );
                  if (cardsPositionInfo.dealedCards[card.hash]) {
                    return (
                      <CardView
                        key={`dealedCard${card.hash}`}
                        card={card}
                        cardImageLoader={this.props.cardImageLoader}
                        isFacingUp
                        zIndex={dealedCards.length - index}
                        position={
                          cardsPositionInfo.dealedCards[card.hash].position
                        }
                      />
                    );
                  }
                  return null;
                })
              ]
            : null}
          {this.renderOfflineGameUi()}
          <AdMobBanner
            adSize="smartBannerLandscape"
            adUnitID={admobBannerUnitID}
            onAdFailedToLoad={error => console.log(error)}
          />
        </View>
      );
    }
    return null;
  };

  renderOfflineGameUi = () => {
    const landlordGameModel = this.state.landlordGameModel;
    let gameUiControls = [
      <Text 
        key="scoreMultiplier"
        style={{
          color: "yellow",
          position: "absolute",
          top: this.windowPaddingOffset + this.cardHeight * 0.3,
          left: this.windowWidth * 0.5 - this.cardWidth * 0.5
        }}
      >
        {`x ${landlordGameModel ? landlordGameModel.scoreMultiplier  : 1}`}
      </Text>,
      <Text
        key="playerPoints"
        style={{
          color: "yellow",
          position: "absolute",
          top: this.windowPaddingOffset + this.cardHeight * 0.3,
          left: this.windowWidth - this.cardWidth * 3
        }}
      >
        {`Score: ${
          typeof this.state.playerPoints !== typeof undefined
            ? this.state.playerPoints
            : 0
        }`}
      </Text>,
      <Button
        transparent
        key="UiControlPauseButton"
        style={{
          position: "absolute",
          top: this.windowPaddingOffset + this.cardHeight * 0.3,
          left: this.windowWidth - this.windowPaddingOffset * 3,
          width: this.windowPaddingOffset * 2
        }}
        onPress={() => {
          if (this.state.flagEffectSound) {
            SoundPickUp.play();
          }
          const playerPoints = parseInt(this.state.playerPoints, 10);
          if (this.state.gamePhase === CONST_GAME_PHASE.gamePhaseEnd) {
            ActionSheet.show(
              {
                options:
                  playerPoints > 0
                    ? ["New Game", "Get more points", "Quit"]
                    : ["Get more points", "Quit"],
                title: "Game ended"
              },
              buttonIndex => {
                if (playerPoints > 0) {
                  if (buttonIndex === 0) {
                    this.initOfflineGame();
                  } else if (buttonIndex === 1) {
                    AdMobRewarded.requestAd().then(() => {
                      AdMobRewarded.showAd();
                    });
                  } else {
                    Actions.menuScene();
                  }
                } else {
                  if (buttonIndex === 0) {
                    AdMobRewarded.requestAd().then(() => {
                      AdMobRewarded.showAd();
                    });
                  } else {
                    Actions.menuScene();
                  }
                }
              }
            );
          } else {
            ActionSheet.show(
              {
                options: ["Get more points", "Quit"],
                title: "Game Paused"
              },
              async buttonIndex => {
                if (buttonIndex === 0) {
                  AdMobRewarded.requestAd().then(() => {
                    AdMobRewarded.showAd();
                  });
                } else {
                  if (
                    this.state.gamePhase === CONST_GAME_PHASE.gamePhaseBid ||
                    this.state.gamePhase === CONST_GAME_PHASE.gamePhasePlay
                  ) {
                    await AsyncStorage.setItem(
                      CONST_PLAYER_POINTS_KEY,
                      JSON.stringify(playerPoints - 300)
                    );
                  }
                  Actions.menuScene();
                }
              }
            );
          }
        }}
      >
        <Icon name="pause" color="blue" fontSize={100} active />
      </Button>
    ];
    if (landlordGameModel) {
      let landlordHatPos;
      if (landlordGameModel.landlord === 0) {
        const posPlayer1Stack = this.cardPositionDictionary.posPlayer1Stack;
        landlordHatPos = new Point(
          posPlayer1Stack.x + this.cardWidth * 0.1,
          this.windowCenterY + this.cardHeight * 0.6
        );
      } else if (landlordGameModel.landlord === 1) {
        const posPlayer2Stack = this.cardPositionDictionary.posPlayer2Stack;
        landlordHatPos = new Point(
          posPlayer2Stack.x + this.cardWidth * 0.1,
          this.windowCenterY + this.cardHeight * 0.6
        );
      } else if (landlordGameModel.landlord === 2) {
        landlordHatPos = new Point(
          this.windowCenterX - this.cardWidth * 0.4,
          this.windowCenterY + this.cardHeight * 0.6
        );
      }

      if (landlordHatPos) {
        gameUiControls = [
          ...gameUiControls,
          <Image
            key="landlordHatImage"
            source={IMG_LANDLORD_HAT}
            resizeMode="contain"
            style={{
              position: "absolute",
              top: landlordHatPos.y,
              left: landlordHatPos.x,
              width: this.cardWidth * 0.8,
              height: this.cardWidth * 0.8
            }}
          />
        ];
      }
      gameUiControls = [
        ...gameUiControls,
        <Text
          key="player1CardCounter"
          style={{
            color: "yellow",
            position: "absolute",
            left: this.cardPositionDictionary.posPlayer1CardCounter.x,
            top: this.cardPositionDictionary.posPlayer1CardCounter.y
          }}
        >
          {landlordGameModel.playerStacks[0].length}
        </Text>,
        <Text
          key="player2CardCounter"
          style={{
            color: "yellow",
            position: "absolute",
            left: this.cardPositionDictionary.posPlayer2CardCounter.x,
            top: this.cardPositionDictionary.posPlayer2CardCounter.y
          }}
        >
          {landlordGameModel.playerStacks[1].length}
        </Text>
      ];
    }

    const player3UiInfo = this.state.player3UiInfo;
    if (player3UiInfo.flagUiControlBidPhaseVisibility) {
      gameUiControls = [
        ...gameUiControls,
        <Button
          key="UiControlPosPairButtonLeftBid"
          light
          rounded
          style={{
            position: "absolute",
            top: this.UiControlPosPairButtonLeft.y,
            left: this.UiControlPosPairButtonLeft.x,
            width: this.cardHeight,
            justifyContent: "center"
          }}
          onPress={() => {
            if (this.state.flagEffectSound) {
              SoundPickUp.play();
            }
            player3UiInfo.flagUiControlBidPhaseVisibility = false;
            this.setState({ player3UiInfo });
            this.gamePhaseHandlerBid("pass");
          }}
        >
          <Text>Pass</Text>
        </Button>,
        <Button
          key="UiControlPosPairButtonRightBid"
          light
          rounded
          style={{
            position: "absolute",
            top: this.UiControlPosPairButtonRight.y,
            left: this.UiControlPosPairButtonRight.x,
            width: this.cardHeight,
            justifyContent: "center"
          }}
          onPress={() => {
            if (this.state.flagEffectSound) {
              SoundPickUp.play();
            }
            player3UiInfo.flagUiControlBidPhaseVisibility = false;
            this.setState({ player3UiInfo });
            this.gamePhaseHandlerBid("bid");
          }}
        >
          <Text>Bid</Text>
        </Button>
      ];
    }

    if (player3UiInfo.flagUiControlPlayPhaseVisibility) {
      gameUiControls = [
        ...gameUiControls,
        <Button
          key="UiControlPosPairButtonLeftPlay"
          light
          rounded
          disabled={!landlordGameModel.previousDealedCardsInfo}
          style={{
            position: "absolute",
            top: this.UiControlPosPairButtonLeft.y,
            left: this.UiControlPosPairButtonLeft.x,
            width: this.cardHeight,
            justifyContent: "center"
          }}
          onPress={() => {
            if (this.state.flagEffectSound) {
              SoundPickUp.play();
            }
            player3UiInfo.flagUiControlPlayPhaseVisibility = false;
            this.setState({ player3UiInfo });
            this.gamePhaseHandlerPlay([]);
          }}
        >
          <Text>Pass</Text>
        </Button>,
        <Button
          key="UiControlPosPairButtonRightPlay"
          light
          rounded
          disabled={!player3UiInfo.flagUiControlPlayPhaseGoButtonEnabled}
          style={{
            position: "absolute",
            top: this.UiControlPosPairButtonRight.y,
            left: this.UiControlPosPairButtonRight.x,
            width: this.cardHeight,
            justifyContent: "center"
          }}
          onPress={() => {
            if (this.state.flagEffectSound) {
              SoundPickUp.play();
            }
            const cardsToDeal = [];
            landlordGameModel.playerStacks[2].map(checkCard => {
              if (checkCard.userSelected) {
                cardsToDeal.push(checkCard);
                checkCard.userSelected = false;
              }
            });
            player3UiInfo.flagUiControlPlayPhaseVisibility = false;
            player3UiInfo.flagUiControlPlayPhaseGoButtonEnabled = false;
            this.setState({ player3UiInfo, landlordGameModel });
            this.gamePhaseHandlerPlay(cardsToDeal.map(card => card.hash));
          }}
        >
          <Text>Go</Text>
        </Button>
      ];
    }

    if (this.state.player1UiInfo.timer > 0) {
      gameUiControls = [
        ...gameUiControls,
        <Text
          key="player1Timer"
          style={{
            color: "white",
            position: "absolute",
            left: this.cardPositionDictionary.posPlayer1Stack.x,
            top:
              this.cardPositionDictionary.posPlayer1Stack.y +
              1.2 * this.cardHeight
          }}
        >
          {this.state.player1UiInfo.timer}
        </Text>
      ];
    }
    if (this.state.player1UiInfo.message !== "") {
      gameUiControls = [
        ...gameUiControls,
        <Text
          key="player1Message"
          style={{
            color: "white",
            fontSize: 18,
            position: "absolute",
            left:
              this.cardPositionDictionary.posPlayer1Stack.x +
              this.cardWidth * 0.1,
            top:
              this.cardPositionDictionary.posPlayer1Stack.y -
              0.3 * this.cardHeight
          }}
        >
          {this.state.player1UiInfo.message.toUpperCase()}
        </Text>
      ];
    }
    if (this.state.player2UiInfo.timer > 0) {
      gameUiControls = [
        ...gameUiControls,
        <Text
          key="player2Timer"
          style={{
            color: "white",
            position: "absolute",
            left: this.cardPositionDictionary.posPlayer2Stack.x,
            top:
              this.cardPositionDictionary.posPlayer2Stack.y +
              1.2 * this.cardHeight
          }}
        >
          {this.state.player2UiInfo.timer}
        </Text>
      ];
    }
    if (this.state.player2UiInfo.message !== "") {
      gameUiControls = [
        ...gameUiControls,
        <Text
          key="player2Message"
          style={{
            color: "white",
            fontSize: 18,
            position: "absolute",
            left:
              this.cardPositionDictionary.posPlayer2Stack.x +
              this.cardWidth * 0.1,
            top:
              this.cardPositionDictionary.posPlayer2Stack.y -
              0.3 * this.cardHeight
          }}
        >
          {this.state.player2UiInfo.message.toUpperCase()}
        </Text>
      ];
    }
    if (gameUiControls.length > 0) {
      return gameUiControls;
    }
    return null;
  };

  render() {
    switch (this.props.GameState.gameType) {
      case CONST_GAME_TYPE.gameTypeOffline:
        return this.renderOfflineGame();
      default:
        return null;
    }
  }
}
GameScene.propTypes = {
  cardImageLoader: PropTypes.object.isRequired,
  GameState: PropTypes.object.isRequired
};
const mapStateToProps = createStructuredSelector({
  GlobalState: createSelector(
    state => state.get("Global"),
    substate => substate.toJS()
  ),
  GameState: createSelector(
    state => state.get("Game"),
    substate => substate.toJS()
  )
});

const mapDispatchToProps = dispatch => ({
  setGamePhase: gamePhase => dispatch(actionSetGamePhase(gamePhase))
});

export default connect(mapStateToProps, mapDispatchToProps)(GameScene);
