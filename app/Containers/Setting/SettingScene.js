import React from "react";
import PropTypes from "prop-types";
import { AsyncStorage } from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import {
  Container,
  Content,
  ListItem,
  CheckBox,
  Body,
  Text,
  Button,
  H1
} from "native-base";
import { actionSetBackgroundMusicFlag } from "../../Redux/Actions/Global.actions";
import { SoundPickUp } from "../../Assets/Sounds/SoundDictionary";

const CONST_FLAG_BACKGROUND_MUSIC_KEY = "FightLandlord:flagBackgroundMusicKey";
const CONST_FLAG_EFFECT_SOUND_KEY = "FightLandlord:flagEffectSoundKey";

class SettingScene extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flagBackgroundMusic: true,
      flagEffectSound: true
    };
  }
  componentDidMount() {
    this.getFlags();
  }

  getFlags = async () => {
    try {
      let flagBackgroundMusic = await AsyncStorage.getItem(
        CONST_FLAG_BACKGROUND_MUSIC_KEY
      );
      if (flagBackgroundMusic === null) {
        flagBackgroundMusic = JSON.stringify(true);
        await AsyncStorage.setItem(
          CONST_FLAG_BACKGROUND_MUSIC_KEY,
          flagBackgroundMusic
        );
      }
      let flagEffectSound = await AsyncStorage.getItem(
        CONST_FLAG_EFFECT_SOUND_KEY
      );
      if (flagEffectSound === null) {
        flagEffectSound = JSON.stringify(true);
        await AsyncStorage.setItem(
          CONST_FLAG_EFFECT_SOUND_KEY,
          flagEffectSound
        );
      }
      // console.log("[getFlags]: ", flagBackgroundMusic, flagEffectSound);
      this.setState({
        flagBackgroundMusic: JSON.parse(flagBackgroundMusic),
        flagEffectSound: JSON.parse(flagEffectSound)
      });
    } catch (err) {
      console.log("[getFlags]: ", err);
    }
  };

  setFlag = async (key, value) => {
    try {
      await AsyncStorage.setItem(
        `FightLandlord:${key}Key`,
        JSON.stringify(value)
      );
      const updatedState = {};
      updatedState[key] = value;
      this.setState(updatedState);
    } catch (err) {
      console.log("[setFlag]: ", err);
    }
  };

  render() {
    return (
      <Container>
        <Content>
          <H1 style={{ textAlign: "center", marginTop: 20 }}>Settings</H1>
          <ListItem>
            <CheckBox
              checked={this.state.flagBackgroundMusic}
              onPress={async () => {
                if (this.state.flagEffectSound) {
                  SoundPickUp.play();
                }
                const flagBackgroundMusic = this.state.flagBackgroundMusic;
                await this.setFlag("flagBackgroundMusic", !flagBackgroundMusic);
                this.props.setBackgroundMusicFlag(!flagBackgroundMusic);
              }}
            />
            <Body>
              <Text>Background music</Text>
            </Body>
          </ListItem>
          <ListItem>
            <CheckBox
              checked={this.state.flagEffectSound}
              onPress={() => {
                const flagEffectSound = this.state.flagEffectSound;
                if (!flagEffectSound) {
                  SoundPickUp.play();
                }
                this.setFlag("flagEffectSound", !flagEffectSound);
              }}
            />
            <Body>
              <Text>Sound effect</Text>
            </Body>
          </ListItem>
          <ListItem>
            <Body>
              <Button
                bordered
                block
                onPress={() => {
                  Actions.menuScene();
                }}
              >
                <Text>OK</Text>
              </Button>
            </Body>
          </ListItem>
        </Content>
      </Container>
    );
  }
}
SettingScene.propTypes = {
  setBackgroundMusicFlag: PropTypes.func.isRequired
};
const mapDispatchToProps = dispatch => ({
  setBackgroundMusicFlag: flag => dispatch(actionSetBackgroundMusicFlag(flag))
});

export default connect(null, mapDispatchToProps)(SettingScene);
