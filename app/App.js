/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import "babel-polyfill";
import React from "react";
import { AsyncStorage } from "react-native";
import { Root } from "native-base";
import { Scene, Router, ActionConst } from "react-native-router-flux";
import { createSelector } from "reselect";
import { connect, Provider } from "react-redux";
import { createStore } from "redux"; // applyMiddleware
// import createSagaMiddleware from "redux-saga";
import Orientation from "react-native-orientation";
import Sound from "react-native-sound";

import AppReducer from "./Redux/Reducers/App.reducer";

// import GameSage from "./Redux/Sagas/Game.saga";

import MenuScene from "./Containers/Menu/MenuScene";
import GameScene from "./Containers/Game/GameScene";
import SettingScene from "./Containers/Setting/SettingScene";

// const sagaMiddleware = createSagaMiddleware();
const store = createStore(AppReducer); // , applyMiddleware(sagaMiddleware)

// sagaMiddleware.run(...GameSage);

const CONST_FLAG_BACKGROUND_MUSIC_KEY = "FightLandlord:flagBackgroundMusicKey";
const CONST_FLAG_EFFECT_SOUND_KEY = "FightLandlord:flagEffectSoundKey";
// eslint-disable-next-line
export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.storeSubscriber = null;
  }

  componentWillMount() {
    Orientation.lockToLandscape();
    this.storeSubscriber = store.subscribe(() => {
      const globalState = createSelector(
        state => state.get("Global"),
        substate => substate.toJS()
      )(store.getState());
      // console.log("[globalState]: ", globalState);
      this.getFlags();
    });
    this.soundBackgroundMusic = new Sound(
      "backgroundmusic.mp3",
      Sound.MAIN_BUNDLE,
      () => {
        this.getFlags();
      }
    );
  }

  componentWillUnmount() {
    if (this.storeSubscriber) {
      this.storeSubscriber();
    }
  }

  getFlags = async () => {
    try {
      let flagBackgroundMusic = await AsyncStorage.getItem(
        CONST_FLAG_BACKGROUND_MUSIC_KEY
      );
      if (flagBackgroundMusic === null) {
        flagBackgroundMusic = JSON.stringify(true);
        await AsyncStorage.setItem(
          CONST_FLAG_BACKGROUND_MUSIC_KEY,
          flagBackgroundMusic
        );
      }
      const flagEffectSoundKey = await AsyncStorage.getItem(
        CONST_FLAG_EFFECT_SOUND_KEY
      );
      if (flagEffectSoundKey === null) {
        await AsyncStorage.setItem(
          CONST_FLAG_EFFECT_SOUND_KEY,
          JSON.stringify(true)
        );
      }
      flagBackgroundMusic = JSON.parse(flagBackgroundMusic);
      // console.log("[flagBackgroundMusic]: ", flagBackgroundMusic);
      if (this.soundBackgroundMusic) {
        this.soundBackgroundMusic.stop();
      }
      if (flagBackgroundMusic) {
        this.soundBackgroundMusicPlayer();
      }
    } catch (err) {
      console.log("[getFlags]: ", err);
    }
  };

  soundBackgroundMusicPlayer = async () => {
    const flagBackgroundMusic = AsyncStorage.getItem(
      CONST_FLAG_BACKGROUND_MUSIC_KEY
    );
    if (this.soundBackgroundMusic && flagBackgroundMusic) {
      this.soundBackgroundMusic.play(() => {
        this.soundBackgroundMusicPlayer();
      });
    }
  };

  render() {
    return (
      <Root>
        <Provider store={store}>
          <Router wrapBy={connect()}>
            <Scene key="root">
              <Scene
                key="menuScene"
                component={MenuScene}
                title="Welcome"
                hideNavBar
                initial
                type={ActionConst.RESET}
              />
              <Scene
                key="gameScene"
                component={GameScene}
                title="Fight Landlord"
                hideNavBar
                type={ActionConst.RESET}
              />
              <Scene
                key="settingScene"
                component={SettingScene}
                title="Settings"
                hideNavBar
                type={ActionConst.RESET}
              />
            </Scene>
          </Router>
        </Provider>
      </Root>
    );
  }
}
