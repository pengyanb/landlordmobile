import Card from "./Card";

const debug = require("debug")("LandlordGame:LandlordGame");
const _ = require("lodash");

// const moment = require("moment");
const rankCount = 13;
const suitCount = 4;

export const EnumDealCardsType = {
  INVALID: "invalid",
  SINGLE: "singles", // type 1: singles
  PAIR: "pairs", // type 2: pairs
  TRIPLE: "triples", // type 3: triples
  BOMB: "bomb", // type 4: bombs
  ROCKET: "rocket", // type 5: rockets
  TRIPLE_PLUS_1: "triplePlus1", // type 6: triplesPlus1
  TRIPLE_PLUS_PAIR: "triplePlusPair", // type 7: triple plus pair
  QUAD_PLUS_2: "quadPlus2", // type 8: quadPlus2
  QUAD_PLUS_2PAIR: "quadPlus2Pair", // type 9: quadPlus2Pair
  FLUSH: "flush", // type 10: flush
  PAIR_CHAIN: "pairChain", // type 11: pair chain
  TRIPLE_CHAIN: "tripleChain", // type 12: triple chain
  TRIPLE_CHAIN_PLUS_1: "tripleChainPlus1", // type 13: triple chain plus 1
  TRIPLE_CHAIN_PLUS_PAIR: "tripleChainPlusPair" // type 14: triple chain plus pair
};

export const EnumDealCardsResult = {
  GAME_FINISHED: "gameFinished",
  GAME_VALID_MOVE: "gameValidMove",
  GAME_INVALID_MOVE: "gameInvalidMove"
};

export const EnumGameType = {
  GAME_TYPE_MANUAL: 1,
  GAME_TYPE_HARDCODEPLAYER: 2,
  GAME_TYPE_AIPLAYER: 3,
  GAME_TYPE_ONLINEPLAYER: 4
};

export default class LandlordGame {
  constructor(gameType, stack, bidLandlordTurn) {
    this.stack = [];
    this.player1Stack = [];
    this.player2Stack = [];
    this.player3Stack = [];
    this.baseStack = [];

    this.landlord = null;
    this.winner = null;
    this.scoreMultiplier = 1;

    this.bidder = null;
    this.initialBidder = null;
    this.bidderValidities = [true, true, true];
    this.bidLandlordTurn = bidLandlordTurn || Math.floor(Math.random() * 3);
    this.bidRoundCount = 0;

    this.playLandlordTurn = null;
    this.previousDealedCardsInfo = null;

    this.gameInfo = {
      stack: [],
      bidLandlordTurn,
      bidLandlordInfo: [],
      dealedCardsInfo: [],
      winner: -1
    };

    this.modelStacksChangeEventHandler = async () => {}; // default function

    if (stack) {
      this.stack = stack.map(
        cardInfo => new Card(cardInfo.rank, cardInfo.suit)
      );
    } else {
      // fill card stack
      for (let suit = 1; suit <= suitCount; suit++) {
        for (let rank = 1; rank <= rankCount; rank++) {
          this.stack.push(new Card(rank, suit));
        }
      }
      this.stack.push(new Card(14, 0));
      this.stack.push(new Card(15, 0));
      this.stack = this.shuffle(this.stack);
    }

    this.gameInfo.stack = _.cloneDeep(this.stack);

    this.playerStacks = [];

    this.playerStacks[0] = _.sortBy(this.stack.splice(0, 17), ["hash"]);
    this.playerStacks[1] = _.sortBy(this.stack.splice(0, 17), ["hash"]);
    this.playerStacks[2] = _.sortBy(this.stack.splice(0, 17), ["hash"]);
    this.baseStack = _.sortBy(this.stack.splice(0, 3), ["hash"]);

    debug("[Stack]: ", this.stack);
    debug("[player1Stacks]: ", this.playerStacks[0]);
    debug("[player2Stack]: ", this.playerStacks[1]);
    debug("[player3Stack]: ", this.playerStacks[2]);
    debug("[baseStack]: ", this.baseStack);

    debug("[bidLandlordTurn]: ", this.bidLandlordTurn);
  }

  bidLandlord = async bid => {
    // return value: true = can continue bid, false = bid process ended
    if (this.landlord === null) {
      const bidderValidity = this.bidderValidities[this.bidLandlordTurn];
      if (bidderValidity) {
        // if player is allowed to bid
        const previousBidder = this.bidder;
        if (bid) {
          this.bidder = this.bidLandlordTurn;
          if (this.initialBidder === null) {
            this.initialBidder = this.bidder;
          } else {
            this.scoreMultiplier *= 2;
          }
        } else {
          // player pass, not valid for proceeding bid
          this.bidderValidities[this.bidLandlordTurn] = false;
        }
        this.gameInfo.bidLandlordInfo.push({
          bidLandlordTurn: this.bidLandlordTurn,
          bid,
          bidRoundCount: this.bidRoundCount,
          previousBidder
        });
      }

      if (this.bidRoundCount === 2) {
        // one round
        if (this.initialBidder === null) {
          // no one bid
          return false;
        } else if (this.bidder === this.initialBidder) {
          // only one player bid
          this.landlord = this.bidder;
          this.playerStacks[this.landlord] = _.sortBy(
            this.playerStacks[this.landlord].concat(
              _.cloneDeep(this.baseStack)
            ),
            ["hash"]
          );
          // this.baseStack = [];
          this.gameInfo.landlord = this.landlord;
          await this.modelStacksChangeEventHandler(this.landlord);
          return false;
        }
      } else if (this.bidRoundCount > 2 && this.bidRoundCount < 6) {
        if (
          this.bidLandlordTurn === this.initialBidder &&
          this.bidder === this.bidLandlordTurn
        ) {
          this.landlord = this.bidLandlordTurn;
          this.playerStacks[this.landlord] = _.sortBy(
            this.playerStacks[this.landlord].concat(
              _.cloneDeep(this.baseStack)
            ),
            ["hash"]
          );
          // this.baseStack = [];
          this.gameInfo.landlord = this.landlord;
          await this.modelStacksChangeEventHandler(this.landlord);
          return false;
        }
        const initialBidderValidity = this.bidderValidities[this.initialBidder];
        if (!initialBidderValidity && bid) {
          // if initial bidder pass and current player bid
          this.landlord = this.bidLandlordTurn;
          this.playerStacks[this.landlord] = _.sortBy(
            this.playerStacks[this.landlord].concat(
              _.cloneDeep(this.baseStack)
            ),
            ["hash"]
          );
          // this.baseStack = [];
          this.gameInfo.landlord = this.landlord;
          return false;
        }
      } else if (this.bidRoundCount >= 6) {
        this.landlord = this.bidder;
        this.playerStacks[this.landlord] = _.sortBy(
          this.playerStacks[this.landlord].concat(_.cloneDeep(this.baseStack)),
          ["hash"]
        );
        // this.baseStack = [];
        this.gameInfo.landlord = this.landlord;
        await this.modelStacksChangeEventHandler(this.landlord);
        return false;
      }

      let foundNext = false;
      let findLoopCount = 0;
      do {
        this.bidLandlordTurn += 1; // next bidder
        if (this.bidLandlordTurn > 2) {
          this.bidLandlordTurn = 0;
        }
        this.bidRoundCount += 1; // increment bid round count
        foundNext = this.bidderValidities[this.bidLandlordTurn];
        if (findLoopCount > 2) {
          return false;
        }
        findLoopCount++;
      } while (!foundNext);
      return true;
    }
    this.gameInfo.landlord = this.landlord;
    return false;
  };

  dealCards = async dealedCardHashs => {
    // return true = game finished, false = game continue
    if (dealedCardHashs.length === 0) {
      // current player has no card to deal
      if (this.previousDealedCardsInfo !== null) {
        // previously dealed cards avaliable
        this.gameInfo.dealedCardsInfo.push({
          playLandlordTurn: this.playLandlordTurn,
          dealedCards: dealedCardHashs
        });
        this.nextRound();
        return EnumDealCardsResult.GAME_VALID_MOVE;
      }
      return EnumDealCardsResult.GAME_INVALID_MOVE;
    }
    const currentPlayerStack = this.playerStacks[this.playLandlordTurn];
    let dealedCards = currentPlayerStack.filter(card =>
      _.includes(dealedCardHashs, card.hash)
    );
    if (dealedCards.length === 1) {
      dealedCards = dealedCards[0];
    }
    const dealedCardsType = LandlordGame.getDealedCardsType(dealedCards);
    if (dealedCardsType === EnumDealCardsType.INVALID) {
      return EnumDealCardsResult.GAME_INVALID_MOVE;
    }
    if (this.previousDealedCardsInfo === null) {
      if(dealedCardsType === EnumDealCardsType.BOMB || dealedCardsType === EnumDealCardsType.ROCKET) {
        this.scoreMultiplier *= 2;
      }
      await this.modelStacksChangeEventHandler(
        this.playLandlordTurn,
        dealedCards
      );
      this.previousDealedCardsInfo = {
        player: this.playLandlordTurn,
        cardsType: dealedCardsType,
        cards: dealedCards
      };
      this.playerStacks[
        this.playLandlordTurn
      ] = LandlordGame.removeDealedCardsFromStack(
        this.playerStacks[this.playLandlordTurn],
        dealedCards
      );
      if (this.playerStacks[this.playLandlordTurn].length === 0) {
        this.winner = this.playLandlordTurn;
        this.gameInfo.dealedCardsInfo.push({
          playLandlordTurn: this.playLandlordTurn,
          dealedCards: dealedCardHashs
        });
        this.gameInfo.winner = this.winner;
        this.gameFinished();
        return EnumDealCardsResult.GAME_FINISHED;
      }
      this.gameInfo.dealedCardsInfo.push({
        playLandlordTurn: this.playLandlordTurn,
        dealedCards: dealedCardHashs
      });
      this.nextRound();
      return EnumDealCardsResult.GAME_VALID_MOVE;
    }
    if (
      LandlordGame.compareCardStacks(this.previousDealedCardsInfo, {
        cardsType: dealedCardsType,
        cards: dealedCards
      })
    ) {
      if(dealedCardsType === EnumDealCardsType.BOMB || dealedCardsType === EnumDealCardsType.ROCKET) {
        this.scoreMultiplier *= 2;
      }
      await this.modelStacksChangeEventHandler(
        this.playLandlordTurn,
        dealedCards
      );
      this.previousDealedCardsInfo = {
        player: this.playLandlordTurn,
        cardsType: dealedCardsType,
        cards: dealedCards
      };
      this.playerStacks[
        this.playLandlordTurn
      ] = LandlordGame.removeDealedCardsFromStack(
        this.playerStacks[this.playLandlordTurn],
        dealedCards
      );
      if (this.playerStacks[this.playLandlordTurn].length === 0) {
        this.winner = this.playLandlordTurn;
        this.gameInfo.dealedCardsInfo.push({
          playLandlordTurn: this.playLandlordTurn,
          dealedCards: dealedCardHashs
        });
        this.gameInfo.winner = this.winner;
        this.gameFinished();
        return EnumDealCardsResult.GAME_FINISHED;
      }
      this.gameInfo.dealedCardsInfo.push({
        playLandlordTurn: this.playLandlordTurn,
        dealedCards: dealedCardHashs
      });
      this.nextRound();
      return EnumDealCardsResult.GAME_VALID_MOVE;
    }
    return EnumDealCardsType.GAME_INVALID_MOVE;
  };

  // ////////////////////////////////////////////////
  //               Utility methods                //
  // ////////////////////////////////////////////////
  shuffle = stack => {
    const shuffled = stack.sort(() => Math.random() - 0.5);
    return shuffled;
  };

  nextRound = () => {
    this.playLandlordTurn += 1;
    if (this.playLandlordTurn > 2) {
      this.playLandlordTurn = 0;
    }
    if (this.previousDealedCardsInfo !== null) {
      if (this.playLandlordTurn === this.previousDealedCardsInfo.player) {
        this.previousDealedCardsInfo = null;
      }
    }
  };

  gameFinished = () => {};

  static removeDealedCardsFromStack = (stack, dealedCards) => {

    if (!Array.isArray(dealedCards)) {
      if (dealedCards instanceof Card) {
        dealedCards = [dealedCards];
      }
    }
    return stack.filter(card => {
      let toKeep = true;
      dealedCards.map(dealedCard => {
        if (dealedCard.hash === card.hash) {
          toKeep = false;
        }
      });
      return toKeep;
    });
  };

  static compareCardStacks = (cardsInfo1, cardsInfo2) => {
    if (Array.isArray(cardsInfo1.cards)) {
      cardsInfo1.cards = _.sortBy(cardsInfo1.cards, ["hash"]);
    }
    if (Array.isArray(cardsInfo2.cards)) {
      cardsInfo2.cards = _.sortBy(cardsInfo2.cards, ["hash"]);
    }
    if (cardsInfo1.cardsType !== cardsInfo2.cardsType) {
      if (cardsInfo2.cardsType === EnumDealCardsType.ROCKET) {
        return true;
      } else if (cardsInfo2.cardsType === EnumDealCardsType.BOMB) {
        if (cardsInfo1.cardsType === EnumDealCardsType.ROCKET) {
          return false;
        }
        return true;
      }
      return false;
    }
    switch (cardsInfo2.cardsType) {
      case EnumDealCardsType.SINGLE:
        if (Array.isArray(cardsInfo2.cards)) {
          cardsInfo2.cards = cardsInfo2.cards[0];
        }
        if (Array.isArray(cardsInfo1.cards)) {
          cardsInfo1.cards = cardsInfo1.cards[0];
        }
        return cardsInfo2.cards.value > cardsInfo1.cards.value;
      case EnumDealCardsType.PAIR:
      case EnumDealCardsType.TRIPLE:
      case EnumDealCardsType.BOMB:
      case EnumDealCardsType.FLUSH:
      case EnumDealCardsType.PAIR_CHAIN:
        if (cardsInfo2.cards.length !== cardsInfo1.cards.length) {
          return false;
        }
        return cardsInfo2.cards[0].value > cardsInfo1.cards[0].value;

      case EnumDealCardsType.TRIPLE_PLUS_1:
      case EnumDealCardsType.TRIPLE_PLUS_PAIR:
      case EnumDealCardsType.TRIPLE_CHAIN_PLUS_1:
      case EnumDealCardsType.TRIPLE_CHAIN_PLUS_PAIR: {
        let cardsInfo1TripleValue = 0;
        let cardsInfo2TripleValue = 0;
        let cardsInfo1Counter = 0;
        let cardsInfo2Counter = 0;
        let cardInfo1PreviousValue = null;
        let cardInfo2PreviousValue = null;
        // debug("[cardsInfo1]: ", JSON.stringify(cardsInfo1, null, 2));
        cardsInfo1.cards.map(card => {
          if (cardInfo1PreviousValue) {
            if (cardInfo1PreviousValue === card.value) {
              cardsInfo1Counter++;
              if (cardsInfo1Counter > 2) {
                cardsInfo1TripleValue = cardInfo1PreviousValue;
                cardsInfo1Counter = 1;
              }
            } else {
              if (cardsInfo1Counter > 2) {
                cardsInfo1TripleValue = cardInfo1PreviousValue;
              }
              cardsInfo1Counter = 1;
            }
          } else {
            cardsInfo1Counter++;
          }
          cardInfo1PreviousValue = card.value;
        });
        cardsInfo2.cards.map(card => {
          if (cardInfo2PreviousValue) {
            if (cardInfo2PreviousValue === card.value) {
              cardsInfo2Counter++;
              if (cardsInfo2Counter > 2) {
                cardsInfo2TripleValue = cardInfo2PreviousValue;
                cardsInfo2Counter = 1;
              }
            } else {
              if (cardsInfo2Counter > 2) {
                cardsInfo2TripleValue = cardInfo2PreviousValue;
              }
              cardsInfo2Counter = 1;
            }
          } else {
            cardsInfo2Counter++;
          }
          cardInfo2PreviousValue = card.value;
        });
        // debug("[cardsInfo2TripleValue]: ", cardsInfo2TripleValue);
        // debug("[cardsInfo1TripleValue]: ", cardsInfo1TripleValue);
        return cardsInfo2TripleValue > cardsInfo1TripleValue;
      }
      case EnumDealCardsType.QUAD_PLUS_2:
      case EnumDealCardsType.QUAD_PLUS_2PAIR: {
        let cardsInfo1QuadValue = 0;
        let cardsInfo2QuadValue = 0;
        let cardsInfo1Counter = 0;
        let cardsInfo2Counter = 0;
        let cardInfo1PreviousValue = null;
        let cardInfo2PreviousValue = null;
        cardsInfo1.cards.map(card => {
          if (cardInfo1PreviousValue) {
            if (cardInfo1PreviousValue === card.value) {
              cardsInfo1Counter++;
              if (cardsInfo1Counter > 3) {
                cardsInfo1QuadValue = cardInfo1PreviousValue;
                cardsInfo1Counter = 1;
              }
            } else {
              if (cardsInfo1Counter > 3) {
                cardsInfo1QuadValue = cardInfo1PreviousValue;
              }
              cardsInfo1Counter = 1;
            }
          } else {
            cardsInfo1Counter++;
          }
          cardInfo1PreviousValue = card.value;
        });
        cardsInfo2.cards.map(card => {
          if (cardInfo2PreviousValue) {
            if (cardInfo2PreviousValue === card.value) {
              cardsInfo2Counter++;
              if (cardsInfo2Counter > 3) {
                cardsInfo2QuadValue = cardInfo2PreviousValue;
                cardsInfo2Counter = 1;
              }
            } else {
              if (cardsInfo2Counter > 3) {
                cardsInfo2QuadValue = cardInfo2PreviousValue;
              }
              cardsInfo2Counter = 1;
            }
          } else {
            cardsInfo2Counter++;
          }
          cardInfo2PreviousValue = card.value;
        });
        return cardsInfo2QuadValue > cardsInfo1QuadValue;
      }
      default:
        return false;
    }
  };

  static getStackCombinations = stack => {
    stack = _.sortBy(stack, ["hash"]);
    const combinationInfo = {};
    // type 1: pairs
    combinationInfo[EnumDealCardsType.PAIR] = [];
    let pairCheckPreviousCard = null;
    stack.map(card => {
      if (pairCheckPreviousCard) {
        if (pairCheckPreviousCard.value === card.value) {
          combinationInfo[EnumDealCardsType.PAIR].push([
            pairCheckPreviousCard,
            card
          ]);
          pairCheckPreviousCard = null;
        } else {
          pairCheckPreviousCard = card;
        }
      } else {
        pairCheckPreviousCard = card;
      }
    });
    // type 2: triples
    combinationInfo[EnumDealCardsType.TRIPLE] = [];
    let tripleCheckPreviousCard1 = null;
    let tripleCheckPreviousCard2 = null;
    stack.map(card => {
      if (
        tripleCheckPreviousCard1 !== null &&
        tripleCheckPreviousCard2 !== null
      ) {
        if (
          tripleCheckPreviousCard1.value === tripleCheckPreviousCard2.value &&
          tripleCheckPreviousCard1.value === card.value
        ) {
          combinationInfo[EnumDealCardsType.TRIPLE].push([
            tripleCheckPreviousCard2,
            tripleCheckPreviousCard1,
            card
          ]);
        }
      }
      tripleCheckPreviousCard2 = tripleCheckPreviousCard1;
      tripleCheckPreviousCard1 = card;
    });
    // type 3: bombs
    combinationInfo[EnumDealCardsType.BOMB] = [];
    let bombCheckPreviousCard1 = null;
    let bombCheckPreviousCard2 = null;
    let bombCheckPreviousCard3 = null;
    stack.map(card => {
      if (
        bombCheckPreviousCard1 !== null &&
        bombCheckPreviousCard2 !== null &&
        bombCheckPreviousCard3 !== null
      ) {
        if (
          bombCheckPreviousCard1.value === bombCheckPreviousCard2.value &&
          bombCheckPreviousCard2.value === bombCheckPreviousCard3.value &&
          bombCheckPreviousCard3.value === card.value
        ) {
          combinationInfo[EnumDealCardsType.BOMB].push([
            bombCheckPreviousCard3,
            bombCheckPreviousCard2,
            bombCheckPreviousCard1,
            card
          ]);
        }
      }
      bombCheckPreviousCard3 = bombCheckPreviousCard2;
      bombCheckPreviousCard2 = bombCheckPreviousCard1;
      bombCheckPreviousCard1 = card;
    });
    // type 4: rockets
    combinationInfo[EnumDealCardsType.ROCKET] = [];
    let rocketjoker = null;
    let rocketJOKER = null;
    stack.map(card => {
      if (card.value === Card.JOKER_MONO_VALUE) {
        rocketjoker = card;
      }
      if (card.value === Card.JOKER_COLOR_VALUE) {
        rocketJOKER = card;
      }
      if (rocketjoker !== null && rocketJOKER !== null) {
        combinationInfo[EnumDealCardsType.ROCKET] = [
          [rocketjoker, rocketJOKER]
        ];
      }
    });

    // type 5: flush
    combinationInfo[EnumDealCardsType.FLUSH] = [];
    let flushChains = [];
    stack.map((card, index) => {
      if (flushChains.length === 0) {
        flushChains.push(card);
      } else {
        let needCheck = false;
        let needReset = false;
        if (flushChains[flushChains.length - 1].value === card.value - 1) {
          flushChains.push(card);
          if (index === stack.length - 1) {
            needCheck = true;
          }
        } else if (flushChains[flushChains.length - 1].value === card.value) {
          if (index === stack.length - 1) {
            needCheck = true;
          }
        } else {
          needCheck = true;
          needReset = true;
        }
        if (needCheck) {
          if (flushChains.length >= 5) {
            combinationInfo[EnumDealCardsType.FLUSH].push(flushChains);
          }
        }
        if (needReset) {
          flushChains = [card];
        }
      }
    });
    // type 6: pair chain
    combinationInfo[EnumDealCardsType.PAIR_CHAIN] = [];
    let pairChains = [];
    combinationInfo[EnumDealCardsType.PAIR].map((pair, index) => {
      if (pairChains.length === 0) {
        pairChains.push(pair);
      } else {
        let needCheck = false;
        let needReset = false;
        if (pairChains[pairChains.length - 1][0].value === pair[0].value - 1) {
          pairChains.push(pair);
          if (index === combinationInfo[EnumDealCardsType.PAIR].length - 1) {
            needCheck = true;
          }
        } else if (
          pairChains[pairChains.length - 1][0].value === pair[0].value
        ) {
          if (index === combinationInfo[EnumDealCardsType.PAIR].length - 1) {
            needCheck = true;
          }
        } else {
          needCheck = true;
          needReset = true;
        }
        if (needCheck) {
          if (pairChains.length >= 3) {
            pairChains = [].concat.apply([], pairChains); // flatted array
            combinationInfo[EnumDealCardsType.PAIR_CHAIN].push(pairChains);
          }
        }
        if (needReset) {
          pairChains = [pair];
        }
      }
    });

    // type 7: triple chain
    combinationInfo[EnumDealCardsType.TRIPLE_CHAIN] = [];
    let tripleChains = [];
    combinationInfo[EnumDealCardsType.TRIPLE].map((triple, index) => {
      if (tripleChains.length === 0) {
        tripleChains.push(triple);
      } else {
        let needCheck = false;
        let needReset = false;
        if (
          tripleChains[tripleChains.length - 1][0].value ===
          triple[0].value - 1
        ) {
          tripleChains.push(triple);
          if (index === combinationInfo[EnumDealCardsType.TRIPLE].length - 1) {
            needCheck = true;
          }
        } else if (
          tripleChains[tripleChains.length - 1][0].value === triple[0].value
        ) {
          if (index === combinationInfo[EnumDealCardsType.TRIPLE].length - 1) {
            needCheck = true;
          }
        } else {
          needCheck = true;
          needReset = true;
        }
        if (needCheck) {
          if (tripleChains.length >= 3) {
            tripleChains = [].concat.apply([], tripleChains); // flatten array
            combinationInfo[EnumDealCardsType.TRIPLE_CHAIN].push(tripleChains);
          }
        }
        if (needReset) {
          tripleChains = [triple];
        }
      }
    });
    // type 8: singles
    combinationInfo[EnumDealCardsType.SINGLE] = [];
    stack.map(card => {
      let isSingleCard = true;
      combinationInfo[EnumDealCardsType.PAIR].map(pair => {
        if (pair[0].hash === card.hash || pair[1].hash === card.hash) {
          isSingleCard = false;
        }
      });
      if (isSingleCard) {
        combinationInfo[EnumDealCardsType.TRIPLE].map(triple => {
          if (
            triple[0].hash === card.hash ||
            triple[1].hash === card.hash ||
            triple[2].hash === card.hash
          ) {
            isSingleCard = false;
          }
        });
      }
      if (isSingleCard) {
        combinationInfo[EnumDealCardsType.BOMB].map(bomb => {
          if (
            bomb[0].hash === card.hash ||
            bomb[1].hash === card.hash ||
            bomb[2].hash === card.hash ||
            bomb[3].hash === card.hash
          ) {
            isSingleCard = false;
          }
        });
      }
      if (isSingleCard) {
        combinationInfo[EnumDealCardsType.ROCKET].map(rocketCard => {
          if (rocketCard.hash === card.hash) {
            isSingleCard = false;
          }
        });
      }
      if (isSingleCard) {
        combinationInfo[EnumDealCardsType.FLUSH].map(flush => {
          flush.map(flushCard => {
            if (flushCard.hash === card.hash) {
              isSingleCard = false;
            }
          });
        });
      }
      if (isSingleCard) {
        combinationInfo[EnumDealCardsType.SINGLE].push(card);
      }
    });
    // type 9: triplesPlus1
    combinationInfo[EnumDealCardsType.TRIPLE_PLUS_1] = [];
    combinationInfo[EnumDealCardsType.TRIPLE].map((triple, index) => {
      if (
        typeof combinationInfo[EnumDealCardsType.SINGLE][index] !==
        typeof undefined
      ) {
        combinationInfo[EnumDealCardsType.TRIPLE_PLUS_1].push([
          ...triple,
          combinationInfo[EnumDealCardsType.SINGLE][index]
        ]);
      }
    });
    // type 10: triplesPlusPair
    combinationInfo[EnumDealCardsType.TRIPLE_PLUS_PAIR] = [];
    let pairOffset = 0;
    combinationInfo[EnumDealCardsType.TRIPLE].map(triple => {
      let pair;
      while (pairOffset < combinationInfo[EnumDealCardsType.PAIR].length) {
        pair = combinationInfo[EnumDealCardsType.PAIR][pairOffset];
        pairOffset += 1;
        if (
          !_.some(triple, { hash: pair[0].hash }) &&
          !_.some(triple, { hash: pair[1].hash })
        ) {
          break;
        } else {
          pair = null;
        }
      }
      if (pair) {
        combinationInfo[EnumDealCardsType.TRIPLE_PLUS_PAIR].push([
          ...triple,
          ...pair
        ]);
      }
    });
    // type 11: quadPlus2
    combinationInfo[EnumDealCardsType.QUAD_PLUS_2] = [];
    combinationInfo[EnumDealCardsType.BOMB].map((bomb, index) => {
      if (
        typeof combinationInfo[EnumDealCardsType.SINGLE][index * 2 + 1] !==
        typeof undefined
      ) {
        combinationInfo[EnumDealCardsType.QUAD_PLUS_2].push([
          ...bomb,
          combinationInfo[EnumDealCardsType.SINGLE][index * 2],
          combinationInfo[EnumDealCardsType.SINGLE][index * 2 + 1]
        ]);
      }
    });
    // type 12: quadPlus2Pair
    combinationInfo[EnumDealCardsType.QUAD_PLUS_2PAIR] = [];
    pairOffset = 0;
    combinationInfo[EnumDealCardsType.BOMB].map(bomb => {
      let pair1;
      let pair2;
      while (pairOffset < combinationInfo[EnumDealCardsType.PAIR].length) {
        const pair = combinationInfo[EnumDealCardsType.PAIR][pairOffset];
        pairOffset += 1;
        if (
          !_.some(bomb, { hash: pair[0].hash }) &&
          !_.some(bomb, { hash: pair[1].hash })
        ) {
          if (!pair1) {
            pair1 = pair;
          } else if (!pair2) {
            pair2 = pair;
            break;
          } else {
            break;
          }
        }
      }
      if (pair1 && pair2) {
        combinationInfo[EnumDealCardsType.QUAD_PLUS_2PAIR].push([
          ...bomb,
          ...pair1,
          ...pair2
        ]);
      }
    });
    // type 13: triple chains plus 1
    combinationInfo[EnumDealCardsType.TRIPLE_CHAIN_PLUS_1] = [];
    let tripleChainOffset = 0;
    combinationInfo[EnumDealCardsType.TRIPLE_CHAIN].map(tripleChain => {
      const tripleCount = tripleChain.length / 3;
      if (
        typeof combinationInfo[EnumDealCardsType.SINGLE][
          tripleChainOffset + tripleCount - 1
        ] !== typeof undefined
      ) {
        const tripleChainPlus1 = [...tripleChain];
        for (let i = tripleChainOffset; i < tripleCount; i++) {
          tripleChainPlus1.push(combinationInfo[EnumDealCardsType.SINGLE][i]);
        }
        combinationInfo[EnumDealCardsType.TRIPLE_CHAIN_PLUS_1].push(
          tripleChainPlus1
        );
        tripleChainOffset += tripleCount;
      }
    });
    // type 14: triple chains plus pair
    combinationInfo[EnumDealCardsType.TRIPLE_CHAIN_PLUS_PAIR] = [];
    pairOffset = 0;
    combinationInfo[EnumDealCardsType.TRIPLE_CHAIN].map(tripleChain => {
      const tripleCount = tripleChain.length / 3;
      const pairs = [];
      while (pairOffset < combinationInfo[EnumDealCardsType.PAIR].length) {
        const pair = combinationInfo[EnumDealCardsType.PAIR][pairOffset];
        pairOffset += 1;
        if (
          !_.some(tripleChain, { hash: pair[0].hash }) &&
          !_.some(tripleChain, { hash: pair[1].hash })
        ) {
          pairs.push(pair);
          if (pairs.length === tripleCount) {
            break;
          }
        }
      }
      if (pairs.length === tripleCount) {
        const tripleChainPlusPair = [...tripleChain];
        pairs.map(pair => {
          tripleChainPlusPair.push(...pair);
        });
        combinationInfo[EnumDealCardsType.TRIPLE_CHAIN_PLUS_PAIR].push(
          tripleChainPlusPair
        );
      }
    });

    return combinationInfo;
  };

  static getDealedCardsType = cards => {
    if (Array.isArray(cards)) {
      if (cards.length === 1) {
        return EnumDealCardsType.SINGLE;
      }
      cards = _.sortBy(cards, ["hash"]);
      if (cards.length === 2) {
        if (cards[0].value === cards[1].value) {
          return EnumDealCardsType.PAIR;
        } else if (
          cards[0].value >= Card.JOKER_MONO_VALUE &&
          cards[1].value >= Card.JOKER_MONO_VALUE
        ) {
          return EnumDealCardsType.ROCKET;
        }
      } else if (cards.length === 3) {
        if (
          cards[0].value === cards[1].value &&
          cards[1].value === cards[2].value
        ) {
          return EnumDealCardsType.TRIPLE;
        }
      } else if (cards.length === 4) {
        if (
          cards[0].value === cards[1].value &&
          cards[1].value === cards[2].value &&
          cards[2].value === cards[3].value
        ) {
          return EnumDealCardsType.BOMB;
        } else if (
          (cards[0].value === cards[1].value &&
            cards[1].value === cards[2].value) ||
          (cards[1].value === cards[2].value &&
            cards[2].value === cards[3].value)
        ) {
          return EnumDealCardsType.TRIPLE_PLUS_1;
        }
      } else if (cards.length > 4) {
        let isFlush = true;
        let previousCard = null;
        let equalRankCount = 1;
        const equalValueCountArray = [];
        cards.map((card, cardIndex) => {
          if (previousCard) {
            if (previousCard.value !== card.value - 1) {
              isFlush = false;
            }
            if (previousCard.value === card.value) {
              equalRankCount++;
              if (cardIndex === cards.length - 1) {
                if (equalRankCount > 1) {
                  equalValueCountArray.push({
                    rank: previousCard.rank,
                    value: previousCard.value,
                    count: equalRankCount
                  });
                }
              }
            } else {
              if (equalRankCount > 1) {
                equalValueCountArray.push({
                  rank: previousCard.rank,
                  value: previousCard.value,
                  count: equalRankCount
                });
              }
              equalRankCount = 1;
            }
          }
          previousCard = card;
        });
        // debug("[equalValueCountArray]: ", equalValueCountArray);
        if (isFlush) {
          return EnumDealCardsType.FLUSH;
        } else if (equalValueCountArray.length > 0) {
          let total = 0;
          let previousPairValue = null;
          let previousTripleValue = null;
          let hasPairChain = true;
          let hasTripleChain = true;
          let hasQuad = false;
          let pairCount = 0;
          let tripleCount = 0;

          equalValueCountArray.map(info => {
            total += info.count;
            if (info.count === 2) {
              pairCount++;
              if (previousPairValue === null) {
                previousPairValue = info.value;
              } else {
                if (previousPairValue !== info.value - 1) {
                  hasPairChain = false;
                }
                previousPairValue = info.value;
              }
            }
            if (info.count === 3) {
              tripleCount++;
              if (previousTripleValue === null) {
                previousTripleValue = info.value;
              } else {
                if (previousTripleValue !== info.value - 1) {
                  hasTripleChain = false;
                }
                previousTripleValue = info.value;
              }
            }
            if (info.count === 4) {
              hasQuad = true;
            }
          });

          if (hasPairChain && pairCount < 3) {
            hasPairChain = false;
          }
          if (hasTripleChain && tripleCount < 2) {
            hasTripleChain = false;
          }
          if (total === cards.length) {
            if (hasTripleChain && tripleCount * 3 === cards.length) {
              return EnumDealCardsType.TRIPLE_CHAIN;
            }
            if (hasTripleChain && tripleCount === pairCount) {
              return EnumDealCardsType.TRIPLE_CHAIN_PLUS_PAIR;
            }
            if(hasTripleChain && tripleCount === pairCount * 2) {
              return EnumDealCardsResult.TRIPLE_CHAIN_PLUS_1;
            }
            if (hasPairChain) {
              return EnumDealCardsType.PAIR_CHAIN;
            }
            if (pairCount === 1 && tripleCount === 1) {
              return EnumDealCardsType.TRIPLE_PLUS_PAIR;
            }
            if (hasQuad) {
              if (pairCount === 2) {
                return EnumDealCardsType.QUAD_PLUS_2PAIR;
              }
            }
          } else {
            if (hasTripleChain) {
              if (cards.length - total === tripleCount) {
                return EnumDealCardsType.TRIPLE_CHAIN_PLUS_1;
              }
            }
            if (hasQuad) {
              if (cards.length - total === 2) {
                return EnumDealCardsType.QUAD_PLUS_2;
              }
            }
          }
        }
      }
    } else if (cards instanceof Card) {
      return EnumDealCardsType.SINGLE;
    }
    return EnumDealCardsType.INVALID;
  };
}
