export const CONST_GAME_TYPE = {
  gameTypeOffline: "OfflineGame",
  gameTypeOnline: "OnlineGame",
  gameTypeSetting: "GameSettings"
};

export const CONST_GAME_PHASE = {
  gamePhaseInitial: "initial",
  gamePhaseDistributingCard: "distributingCards",
  gamePhaseBid: "bid",
  gamePhasePlay: "play",
  gamePhaseEnd: "end",
  gamePhaseConnecting: "connecting"
};
