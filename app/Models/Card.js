export default class Card {
  constructor(rank, suit, hash) {
    this.userSelected = false;
    this.position = null;
    if (hash) {
      this.hash = hash;
      this.suit = hash % 10;
      const rankValue = Math.floor(hash / 10);
      this.value = rankValue;
      if (rankValue === 20) {
        this.rank = 2;
      } else if (rankValue === Card.JOKER_MONO_VALUE) {
        this.rank = 14;
      } else if (rankValue === Card.JOKER_COLOR_VALUE) {
        this.rank = 15;
      } else if (rankValue === 12) {
        this.rank = 1;
      } else {
        this.rank = rankValue + 2;
      }
    } else {
      this.rank = rank;
      this.suit = suit;
      if (rank === 14) {
        this.value = Card.JOKER_MONO_VALUE;
        this.hash = this.value * 10;
      } else if (rank === 15) {
        this.value = Card.JOKER_COLOR_VALUE;
        this.hash = this.value * 10;
      } else if (rank === 2) {
        this.value = 20;
        this.hash = this.value * 10 + suit;
      } else if (rank === 1) {
        this.value = rank + 11;
        this.hash = this.value * 10 + suit;
      } else {
        this.value = rank - 2;
        this.hash = this.value * 10 + suit;
      }
    }
  }
  toString() {
    let suitLabel;
    switch (this.suit) {
      case 1:
        suitLabel = "Spade";
        break;
      case 2:
        suitLabel = "Heart";
        break;
      case 3:
        suitLabel = "Club";
        break;
      case 4:
        suitLabel = "Diamond";
        break;
      default:
        suitLabel = "";
        break;
    }
    if (suitLabel) {
      const rankLabel = this.rank;
      return `${rankLabel}${suitLabel}`;
    }
    if (this.rank === 14) {
      return "jokerMono";
    } else if (this.rank === 15) {
      return "jokerColor";
    }

    return null;
  }
}

Card.JOKER_MONO_VALUE = 30;
Card.JOKER_COLOR_VALUE = 40;
