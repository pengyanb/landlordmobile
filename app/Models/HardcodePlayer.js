import Card from "./Card";
import LandlordGame, { EnumDealCardsType } from "./LandlordGame";

const AGGRESIVE_DEAL_CARD_PRIORITY = [
  EnumDealCardsType.FLUSH,
  EnumDealCardsType.TRIPLE_CHAIN_PLUS_1,
  EnumDealCardsType.TRIPLE_CHAIN_PLUS_PAIR,
  EnumDealCardsType.TRIPLE_CHAIN,
  EnumDealCardsType.PAIR_CHAIN,
  EnumDealCardsType.TRIPLE_PLUS_1,
  EnumDealCardsType.TRIPLE_PLUS_PAIR,
  EnumDealCardsType.TRIPLE,
  EnumDealCardsType.PAIR,
  EnumDealCardsType.SINGLE,
  EnumDealCardsType.BOMB,
  EnumDealCardsType.ROCKET,
  EnumDealCardsType.QUAD_PLUS_2PAIR,
  EnumDealCardsType.QUAD_PLUS_2
];
const NON_AGGRESIVE_DEAL_CARD_PRIORITY = [
  EnumDealCardsType.SINGLE,
  EnumDealCardsType.FLUSH,
  EnumDealCardsType.PAIR_CHAIN,
  EnumDealCardsType.PAIR,
  EnumDealCardsType.TRIPLE_CHAIN_PLUS_1,
  EnumDealCardsType.TRIPLE_CHAIN_PLUS_PAIR,
  EnumDealCardsType.TRIPLE_CHAIN,
  EnumDealCardsType.TRIPLE_PLUS_1,
  EnumDealCardsType.TRIPLE_PLUS_PAIR,
  EnumDealCardsType.TRIPLE,
  EnumDealCardsType.BOMB,
  EnumDealCardsType.ROCKET,
  EnumDealCardsType.QUAD_PLUS_2,
  EnumDealCardsType.QUAD_PLUS_2PAIR
];

export default class HardcodePlayer {
  bid = playerStack => {
    let totalHash = 0;
    playerStack.map(card => {
      totalHash += card.hash;
    });
    // console.log("[HardcodePlayer] totalHash: ", totalHash);
    if (totalHash > 1600) {
      return true;
    }
    return false;
  };

  play = (
    player,
    landlord,
    previousDealedCards,
    playerStack,
    previousPlayerCardsCount,
    nextPlayerCardsCount
  ) => {
    let beAggresive = false;
    let beSoftAggresive = false;
    const playerCombinationInfo = LandlordGame.getStackCombinations(
      playerStack
    );
    let nextPlayer = player + 1;
    if (nextPlayer > 2) {
      nextPlayer = 0;
    }

    if (
      playerStack.length < 6 ||
      previousPlayerCardsCount < 6 ||
      nextPlayerCardsCount < 6
    ) {
      if (nextPlayer === landlord) {
        beAggresive = true;
      }
    }
    if (!beAggresive) {
      if (nextPlayer === landlord) {
        beSoftAggresive = true;
      }
    }

    let cardsToDeal = [];
    // console.log(`[HardcodePlayer ${player + 1}] aggresive: `, beAggresive);
    if (previousDealedCards) {
      const dealedCardsType = LandlordGame.getDealedCardsType(
        previousDealedCards
      );
      if (
        dealedCardsType !== EnumDealCardsType.INVALID &&
        playerCombinationInfo[dealedCardsType].length > 0
      ) {
        if (beAggresive) {
          const dealedCards =
            playerCombinationInfo[dealedCardsType][
              playerCombinationInfo[dealedCardsType].length - 1
            ];
          if (
            LandlordGame.compareCardStacks(
              { cardsType: dealedCardsType, cards: previousDealedCards },
              { cardsType: dealedCardsType, cards: dealedCards }
            )
          ) {
            cardsToDeal = dealedCards;
          } else if (dealedCardsType !== EnumDealCardsType.BOMB) {
            if (playerCombinationInfo[EnumDealCardsType.BOMB].length > 0) {
              cardsToDeal = playerCombinationInfo[EnumDealCardsType.BOMB][0];
            } else if (
              playerCombinationInfo[EnumDealCardsType.ROCKET].length > 0
            ) {
              cardsToDeal = playerCombinationInfo[EnumDealCardsType.ROCKET][0];
            }
          }
        } else {
          for (
            let i = 0;
            i < playerCombinationInfo[dealedCardsType].length;
            i++
          ) {
            const dealedCards = playerCombinationInfo[dealedCardsType][i];
            if (
              LandlordGame.compareCardStacks(
                { cardsType: dealedCardsType, cards: previousDealedCards },
                { cardsType: dealedCardsType, cards: dealedCards }
              )
            ) {
              cardsToDeal = dealedCards;
              break;
            }
          }
        }
      }
    } else {
      if (beAggresive) {
        for (let i = 0; i < AGGRESIVE_DEAL_CARD_PRIORITY.length; i++) {
          const dealedCardsType = AGGRESIVE_DEAL_CARD_PRIORITY[i];
          if (playerCombinationInfo[dealedCardsType].length > 0) {
            cardsToDeal =
              playerCombinationInfo[dealedCardsType][
                playerCombinationInfo[dealedCardsType].length - 1
              ];
            break;
          }
        }
      } else if (beSoftAggresive) {
        for (let i = 0; i < AGGRESIVE_DEAL_CARD_PRIORITY.length; i++) {
          const dealedCardsType = AGGRESIVE_DEAL_CARD_PRIORITY[i];
          if (playerCombinationInfo[dealedCardsType].length > 0) {
            for (
              let j = playerCombinationInfo[dealedCardsType].length - 1;
              j >= 0;
              j--
            ) {
              // console.log("hash 1", dealedCardsType);
              if (dealedCardsType === EnumDealCardsType.SINGLE) {
                if (playerCombinationInfo[dealedCardsType][j].hash < 120) {
                  cardsToDeal = playerCombinationInfo[dealedCardsType][j];
                  break;
                }
              } else {
                if (playerCombinationInfo[dealedCardsType][j][0].hash < 120) {
                  cardsToDeal = playerCombinationInfo[dealedCardsType][j];
                  break;
                }
              }
            }
            if (cardsToDeal.length > 0) {
              break;
            }
          }
        }
      } else {
        for (let i = 0; i < NON_AGGRESIVE_DEAL_CARD_PRIORITY.length; i++) {
          const dealedCardsType = NON_AGGRESIVE_DEAL_CARD_PRIORITY[i];
          if (playerCombinationInfo[dealedCardsType].length > 0) {
            if (dealedCardsType === EnumDealCardsType.SINGLE) {
              if (playerCombinationInfo[dealedCardsType][0].hash < 120) {
                cardsToDeal = playerCombinationInfo[dealedCardsType][0];
                break;
              }
            } else {
              if (playerCombinationInfo[dealedCardsType][0][0].hash < 120) {
                cardsToDeal = playerCombinationInfo[dealedCardsType][0];
                break;
              }
            }
          }
        }
      }

      if (cardsToDeal.length === 0) {
        for (let i = 0; i < NON_AGGRESIVE_DEAL_CARD_PRIORITY.length; i++) {
          const dealedCardsType = NON_AGGRESIVE_DEAL_CARD_PRIORITY[i];
          if (playerCombinationInfo[dealedCardsType].length > 0) {
            cardsToDeal = playerCombinationInfo[dealedCardsType][0];
            break;
          }
        }
      }
    }

    return cardsToDeal;
  };
}
